import React from 'react';
import { View, Text } from 'react-native';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import FlashMessage from "react-native-flash-message";

import Navigator from './src/@navigator';
import Reducer from './src/@reducer';

const middlewares = []

if (process.env.NODE_ENV === 'development') {
  const { logger } = require('redux-logger');
  middlewares.push(logger);
}

const store = createStore(
  Reducer,
  applyMiddleware(...middlewares),
)

export default () => (
  <Provider
    store={store}
  >
    <Navigator />
    <FlashMessage position="top" />
  </Provider>
)
