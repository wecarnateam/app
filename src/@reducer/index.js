import { combineReducers } from 'redux';
import setting from './setting';
import auth from './auth';
import customer from './customer';

export default combineReducers({
    setting,
    auth,
    customer,
});
