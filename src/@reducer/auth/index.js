import { AUTH } from '../../@action/type';

const INTIAL_STATE = {
    type: "basic",
    username: 'admin@wecarna.com',
    password: '12345',
}

export default (state = INTIAL_STATE, action) => {
    switch (action.type) {
        case AUTH:
            return {
                ...state,
                auth: {
                    ...state.auth,
                    ...action.payload,
                }
            }
        default:
            return state
    }
}
