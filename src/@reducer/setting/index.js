import { SETTING, NOTIFICATION } from '../../@action/type';
import * as RNLocalize from 'react-native-localize';

const INTIAL_STATE = {
    api: 'http://api.wecarna.com/ocwebservice/api',
    notification: {},
    language: ['tr', 'tr-TR', 'tr-us'].indexOf(RNLocalize.getLocales().map(item => item.languageCode)) > -1 ? 'tr' : 'en',
}

export default (state = INTIAL_STATE, action) => {
    switch (action.type) {
        case SETTING:
            return {
                ...state,
                ...action.payload
            }
        case NOTIFICATION:
            return {
                ...state,
                notification: {
                    ...state.notification,
                    ...action.payload,
                }
            }
        default:
            return state
    }
}
