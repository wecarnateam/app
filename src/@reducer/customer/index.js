import { CUSTOMER } from '../../@action/type';

const INTIAL_STATE = {
}

export default (state = INTIAL_STATE, action) => {
    switch (action.type) {
        case CUSTOMER:
            return {
                ...state,
                customer: {
                    ...state.customer,
                    ...action.payload,
                }
            }
        default:
            return state
    }
}
