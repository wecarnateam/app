import React, { Component } from 'react';
import { Text, View, SafeAreaView, ScrollView, Dimensions, Image, Button } from 'react-native';
import { createSwitchNavigator, createAppContainer } from 'react-navigation';
import { createStackNavigator, } from 'react-navigation-stack';
import FontAwesomeIcons from 'react-native-vector-icons/FontAwesome';
import { createBottomTabNavigator } from 'react-navigation-tabs';
import { createDrawerNavigator, DrawerItems } from 'react-navigation-drawer';
import { Icon } from 'native-base';

import Home from '../home';
import Category from '../category';
import Product from '../product';
import ProductDetail from '../product/detail';
import ShoppingCart from '../shoppingcart';
import Order from '../order';
import CompleteOrder from '../order/completeorder';
import SignUp from '../user/signup';
import SignIn from '../user';


import ShoppingCartButton from '../component/shoppingcart/navigator_button';
import LoginButton from '../component/navigator/login_button';


const { width } = Dimensions.get("window");

const MainStack = createStackNavigator({
  Home,
  Product,
  ProductDetail,
  CompleteOrder,
}, {
  initialRouteName: 'Home',
  headerMode: 'none',
});

const CustomDrawerNavigation = (props) => {

  return (
    <SafeAreaView style={{ flex: 1 }}>
      <View>
        <LoginButton loginnavigation={() => props.navigation.navigate("SignIn")} />
      </View>
      <ScrollView>
        <DrawerItems {...props} />
      </ScrollView>
      <View style={{ alignItems: "center", bottom: 20 }}>
        <View style={{ flexDirection: 'row' }}>
          <View style={{ flexDirection: 'column', marginRight: 15 }}>
            <Icon name="flask" style={{ fontSize: 24 }} onPress={() => console.log("Tıkladın")} />
          </View>
          <View style={{ flexDirection: 'column' }}>
            <Icon name="call" style={{ fontSize: 24 }} onPress={() => console.log("Tıkladın")} />
          </View>
        </View>
      </View>
    </SafeAreaView>
  );
}



const TabNavigator = createBottomTabNavigator({
  'Home': {
    screen: Home,
    navigationOptions: {
      tabBarLabel: 'Anasayfa',
      tabBarIcon: ((focused, horizontal, tintColor) => {
        let IconComponent = FontAwesomeIcons;
        let iconName = 'home';
        if (focused)
          tintColor = '#FFFFFF';
        return <IconComponent name={iconName} size={25} color={tintColor} />
      })
    }
  },
  'ShoppingCart': {
    screen: ShoppingCart,
    navigationOptions: {
      tabBarLabel: ' ',
      tabBarIcon: ((focused, horizontal, tintColor) => {
        let IconComponent = FontAwesomeIcons;
        let iconName = 'shopping-cart';
        if (focused)
          tintColor = '#FFFFFF';
        return (
          <ShoppingCartButton iconname={iconName} size={25} tintcolor={tintColor} />
        )
      })
    }
  },
  'Order': {
    screen: Order,
    navigationOptions: {
      tabBarLabel: 'Siparişlerim',
      tabBarIcon: ((focused, horizontal, tintColor) => {
        let IconComponent = FontAwesomeIcons;
        let iconName = 'suitcase';
        if (focused)
          tintColor = '#FFFFFF';
        return <IconComponent name={iconName} size={25} color={tintColor} />
      })
    }
  }
}, {
  tabBarOptions: {
    activeTintColor: '#FFFFFF',
    inactiveTintColor: '#397596',
    style: {
      backgroundColor: '#0D0D0D',
    },
    indicatorStyle: {
      backgroundColor: '#0D0D0D',
    },
  }

});


/*

const TabNavigator = createBottomTabNavigator({
  'Home': {
      screen: Home,
      navigationOptions: {
          tabBarLabel: 'Anasayfa',
          tabBarIcon: ((focused, horizontal, tintColor) => {
              let IconComponent = FontAwesomeIcons;
              let iconName = 'home';
              if (focused)
                  tintColor = '#84C1E3';
              return <IconComponent name={iconName} size={25} color={tintColor} />
          })
      }
  },
  'ShoppingCart': {
      screen: ShoppingCart,
      navigationOptions: {
          tabBarLabel: '  ',
          tabBarIcon: ((focused, horizontal, tintColor) => {
              let IconComponent = FontAwesomeIcons;
              let iconName = 'shopping-cart';
              if (focused)
                  tintColor = '#84C1E3';
              return <IconComponent name={iconName} size={25} color={tintColor} />
          })
      }
  },
  'Order': {
      screen: Order,
      navigationOptions: {
          tabBarLabel: 'Siparişlerim',
          tabBarIcon: ((focused, horizontal, tintColor) => {
              let IconComponent = FontAwesomeIcons;
              let iconName = 'suitcase';
              if (focused)
                  tintColor = '#84C1E3';
              return <IconComponent name={iconName} size={25} color={tintColor} />
          })
      }
  },
}, {

}, {
  defaultNavigationOptions: ({ navigation }) => ({
     
  })
});*/


const Drawer = createDrawerNavigator({
  Home: {
    screen: TabNavigator,
    navigationOptions: {
      title: 'Home'
    }
  },
  Category: {
    screen: Category,
    navigationOptions: {
      title: 'Category'
    }
  },
  Product: {
    screen: Product,
    navigationOptions: {
      title: 'Product'
    }
  },
  SignIn: {
    screen: SignIn,
    navigationOptions: {
      title: 'Sign In'
    }
  },
  SignUp: {
    screen: SignUp,
    navigationOptions: {
      title: 'Sign Up'
    }
  }
},
  {
    drawerPosition: 'right',
    contentComponent: CustomDrawerNavigation,


    drawerWidth: (width / 3) * 2
  });




const AppNavigator = createSwitchNavigator({
  MainStack,
  TabNavigator,
  Drawer
}, {
  initialRouteName: 'Drawer'
});



export default createAppContainer(AppNavigator);