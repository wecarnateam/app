import React, { PureComponent } from 'react';
import { StyleSheet, Dimensions } from 'react-native';

export default StyleSheet.create({
    categorybutton: {
        margin: 10,
        paddingTop: 15,
        paddingBottom: 15,
        alignItems: 'center',
        width: ((Dimensions.get('screen').width - 60) / 3),
        backgroundColor: '#FFFFFF',
        borderRadius: 8,
        shadowColor: "#000000",
        shadowOffset: {
            width: 0,
            height: 8,
        },
        shadowOpacity: 0.8,
        shadowRadius: 10,
        elevation: 30,
    },
    title: {
        fontSize: 12,
        fontFamily:'Poppins-Regular',
    },
});
