import React, { PureComponent } from 'react';
import { StyleSheet, } from 'react-native';

export default StyleSheet.create({
    container: {
        paddingTop: 0,
        height: 45,
        backgroundColor: '#FFFFFF',
        fontSize: 20
    },
    title: {
        fontFamily: 'Poppins',
        fontWeight: 'normal',
        fontSize: 18,
        color: '#F23D3D',
    }
});
