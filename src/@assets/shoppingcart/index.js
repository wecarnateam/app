import React, { PureComponent } from 'react';
import { StyleSheet, Dimensions } from 'react-native';

export default StyleSheet.create({
    shoppingcart: {
        borderRadius: 12,
        borderWidth: 0.5,
        borderColor: '#E9EAEE',
        padding: 5,
        margin: 5,
        backgroundColor: '#FFFFFF'
    }
});
