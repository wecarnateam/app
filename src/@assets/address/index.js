import React, { PureComponent } from 'react';
import { StyleSheet, } from 'react-native';

export default StyleSheet.create({
    textinput: {
        padding: 8,
        borderColor: '#CDCDCD',
        borderWidth: 0.5,
        borderRadius: 5,
    }
});
