import React, { PureComponent } from 'react';
import { StyleSheet, Dimensions } from 'react-native';

export default StyleSheet.create({
    categorybutton: {
        borderRadius: 10,
        backgroundColor: '#FFFFFF',
        margin: 5,
        marginLeft: 10,
        padding: 5,
        paddingLeft: 15,
        paddingRight: 15,
        borderWidth: 1,
        shadowColor: "#000000",
        shadowOffset: {
            width: 0,
            height: 8,
        },
        shadowOpacity: 0.2,
        shadowRadius: 10,
        elevation: 3,
    },
    categorybuttontitle: {
        fontSize: 13,
        fontFamily: 'Poppins-Regular',
    },
    chooseItem: {
        backgroundColor: '#226977',
          borderColor:'#757575'
      }
      
      ,
      chooseItem1: {
        backgroundColor: '#757575'
      }
});
