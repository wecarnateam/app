import React, { PureComponent } from 'react';
import { StyleSheet, Dimensions } from 'react-native';
import { Left } from 'native-base';

export default StyleSheet.create({
    ImageFrame: {

    },
    ImageFramePrice: {
      position: 'absolute',
      bottom: 15,
      right: 10,
      color: '#000000',
      backgroundColor: 'rgba(255, 255, 255, 0.7)',
      paddingTop: 5,
      paddingBottom: 5,
      paddingLeft: 10,
      paddingRight: 10,
      borderRadius: 10,
      fontFamily: 'Poppins-Regular',
      fontSize: 18,
      fontWeight: "bold",

      shadowColor: "#000",
      shadowOffset: {
        width: 0,
        height: 1,
      },
      shadowOpacity: 0.22,
      shadowRadius: 2.22,
      
      elevation: 3,
    },
    ImageFrameQuantity: {
      flexDirection: 'row',
      position: 'absolute',
      bottom: 15,
      left: 10,
      backgroundColor: 'rgba(255, 255, 255, 0.7)',
      fontFamily: 'Poppins-Regular',
      fontSize: 16,
      fontWeight: "bold",
      borderRadius: 10,
      shadowColor: "#000",
      shadowOffset: {
        width: 0,
        height: 1,
      },
      shadowOpacity: 0.22,
      shadowRadius: 2.22,
      
      elevation: 3,
    },
    categorybutton: {
        
    },
    categorybuttontitle: {
        fontSize: 13,
        fontFamily: 'Poppins-Regular',
    },
    OptionLabel: {
        fontSize: 16,
        color: '#397596',
        fontWeight: "bold",
        fontFamily: 'Poppins-Regular',
        marginLeft: 15,
        marginRight: 15,
        marginTop: 10,
        marginBottom: 5
    },
    OptionLabelOverline : {
      textDecorationLine: 'line-through'
    },
    OptionGroup: {
        marginLeft: 10,
        marginRight: 10
    },
    OptionItem: {
        backgroundColor: '#FFFFFF',
        borderRadius: 10,
        color: '#0D0D0D',
        fontSize: 12,
        margin: 5,
        padding: 8,
        borderColor: '#FFFFFF',
        borderWidth: 2
      },
    OptionItemActive: {
        backgroundColor: '#FFFFFF',
        color: '#0D0D0D',
        fontSize: 12,
        margin: 5,
        padding: 8,
        borderRadius: 10,
        borderColor: '#84C1E3',
        borderWidth: 2
      },
      CartButton: {
        textAlign: "center",
        fontSize: 16,
        color: '#FFFFFF',
        backgroundColor: '#01CD4C',
        paddingTop: 20,
        paddingBottom: 20,
      },
      CartButtonText: {
        color: '#FFFFFF',
        textAlign: "center",
      },
      MultiSelect: {
        margin: 5,
        padding: 8,
        borderRadius: 10
      },
      MultiSelectMainWrapper: {
       
      },
      MultiSelectInputGroup: {
        
      },
      MultiSelectItemsContainer: {
       
        
      },
      MultiSelectListContainer: {
       
      },
      MultiSelectRowList: {
        
      },
      MultiSelectSelectorContainer: {
        backgroundColor: '#000000',
        
      },
      MultiSelectDropdownMenu: {
        backgroundColor: '#000000',
        
      },
      MultiSelectTextDropdown: {
        padding: 8,
        
      },
      MultiSelectTextDropdownSelected: {
        
      }

});
