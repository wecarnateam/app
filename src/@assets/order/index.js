import React, { PureComponent } from 'react';
import { StyleSheet, Dimensions } from 'react-native';

export default StyleSheet.create({
    container: {
        backgroundColor: '#FFFFFF',
        borderRadius: 15,
        marginBottom: 15
    },
    titlecontainer: {
        flexDirection: 'row',
        paddingBottom: 15,
        paddingTop: 20,
        paddingLeft: 30,
        paddingRight: 30,
        borderBottomWidth: 2,
        borderBottomColor: '#E9EAEE'
    },
    ordertitle: {
        fontFamily: 'Poppins-Regular',
        fontSize: 18,
        fontWeight: 'bold',
        color: '#3F3F3F',
        alignSelf: 'flex-start',
        flex: 1,
    },
    orderstatus: {
        fontFamily: 'Poppins-Regular',
        fontSize: 18,
        fontWeight: 'bold',
        color: '#A18B1D',
        alignSelf: 'flex-end',
        width: 100,
        textAlign: 'right',
    },
    orderstatuscomplete: {
        fontFamily: 'Poppins-Regular',
        fontSize: 18,
        fontWeight: 'bold',
        color: '#4B9648',
        alignSelf: 'flex-end',
        width: 100,
        textAlign: 'right',
    },
    productmaincontainer: {
        paddingBottom: 15,
        paddingTop: 20,
        paddingLeft: 30,
        paddingRight: 30,
    },
    productcontainer: {
        flexDirection: 'row',
        paddingBottom: 5,
    },
    producttitle: {
        fontFamily: 'Poppins-Regular',
        fontSize: 16,
        color: '#3F3F3F',
        alignSelf: 'flex-start',
        flex: 1,
    },
    productcount: {
        fontFamily: 'Poppins-Regular',
        fontSize: 16,
        color: '#3F3F3F',
        alignSelf: 'flex-end'
    },
    pricecontainer: {
        paddingBottom: 15,
        paddingLeft: 30,
        paddingRight: 30,
    },
    pricetitle: {
        alignSelf: 'flex-end',
        fontFamily: 'Poppins-Regular',
        fontSize: 18,
        fontWeight: 'bold',
        color: '#3F3F3F',
    }
});
