import React, { PureComponent } from 'react';
import { FlatList, View, Text, TouchableOpacity, ScrollView, Dimensions, RefreshControl, ActivityIndicator, } from 'react-native';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Button, ButtonGroup, Header, Image, } from 'react-native-elements';
import FontAwesomeIcons from 'react-native-vector-icons/FontAwesome';
import { DrawerActions } from 'react-navigation-drawer';

import { GET, POST } from '../@helper/helper';
import HeaderStyles from '../@assets/header';
import ShoppingCartStyles from '../@assets/shoppingcart';
import StatusBar from '../component/statusbar';
import { WEB_PAGE_URL } from '../static/variables';

const screenWidthHeight = {
    width: Dimensions.get('screen').width,
    height: Dimensions.get('screen').height,
}

class Index extends PureComponent {
    static propTypes = {
        navigation: PropTypes.object.isRequired,
    }

    constructor(props) {
        super(props);
        this.state = {
            cartlist: null,
            refreshing: false,
            isquantityup: false,
            isquantitydown: false,
            cartModel: null,
            cartTotalPrice: 0,
            ...props.navigation.state.params,
        }
    }

    getCartList() {
        this.setState({
            refreshing: true,
        });
        GET('/login', {
            setting: this.props.setting,
            authorization: this.props.auth,
        })
            .then((response) => response.json())
            .then((responseJson) => {
                console.log(responseJson);
                POST('/cartlistbycustomer', {
                    setting: this.props.setting,
                    headers: {
                        'x-access-token': responseJson.token
                    },
                    data: {
                        customer_id: this.props.customer.customer.customer.customer_id,
                    },
                })
                    .then((response) => response.json())
                    .then((responseJson) => {
                        this.setState({
                            cartlist: responseJson.cartlist,
                            refreshing: false,
                            isquantitydown: false,
                            isquantityup: false,
                        });
                        let propsCustomer = this.props.customer.customer;
                        propsCustomer.cartcount = responseJson.cartlist.length;
                        this.props.onLogin(propsCustomer);
                    })
                    .catch((error) => {
                        console.log(error);
                    });
            })
            .catch((error) => {
                console.log(error);
            });
    }

    deleteCartFromList(cartItem) {
        this.setState({
            cartlist: null,
            refreshing: true,
        });
        GET('/login', {
            setting: this.props.setting,
            authorization: this.props.auth,
        })
            .then((response) => response.json())
            .then((responseJson) => {
                console.log(responseJson);
                POST('/deletecart', {
                    setting: this.props.setting,
                    headers: {
                        'x-access-token': responseJson.token
                    },
                    data: {
                        cart_id: cartItem.cart_id,
                        customer_id: cartItem.customer_id
                    },
                })
                    .then((response) => response.json())
                    .then((responseJson) => {
                        console.log(responseJson, "silindiiii");
                        this.refreshCartList();
                    })
                    .catch((error) => {
                        console.log(error);
                    });
            })
            .catch((error) => {
                console.log(error);
            });
    }

    getTotalCartPrice() {
        let cartTotal = 0;
        if (this.state.cartlist) {
            this.state.cartlist.map((cart, index) => {
                let cartPrice = parseFloat(cart.product.price);
                cart.product_option_list.map((proOpt, index) => {
                    proOpt.product_option_value_list.map((proOptVal, index) => {
                        cartPrice += parseFloat(proOptVal.price);
                    });
                });
                cartTotal += cartPrice * cart.quantity;
            });
        }

        return cartTotal.toFixed(2);
    }

    getCartPrice(cartItem) {
        let total = parseFloat(cartItem.product.price);
        cartItem.product_option_list.map((proOpt, index) => {
            proOpt.product_option_value_list.map((proOptVal, index) => {
                total += parseFloat(proOptVal.price);
            });
        });

        return (total * cartItem.quantity).toFixed(2);
    }

    updateCart(cartItem) {
        this.setState({
            refreshing: true,
        });
        GET('/login', {
            setting: this.props.setting,
            authorization: this.props.auth,
        })
            .then((response) => response.json())
            .then((responseJson) => {
                console.log(responseJson);
                POST('/cart', {
                    setting: this.props.setting,
                    headers: {
                        'x-access-token': responseJson.token
                    },
                    data: {
                        ...cartItem
                    },
                })
                    .then((response) => response.json())
                    .then((responseJson) => {
                        this.refreshCartList()
                    })
                    .catch((error) => {
                        console.log(error);
                    });
            })
            .catch((error) => {
                console.log(error);
            });
    }

    cartProductQuantityDown(cartItem) {
        if (cartItem.quantity > 1) {
            this.setState({
                cartModel: cartItem,
                isquantitydown: true,
            });
            this.updateCart({
                "cart_id": cartItem.cart_id,
                "customer_id": cartItem.customer_id,
                "session_id": cartItem.session_id,
                "product_id": cartItem.product_id,
                "option": cartItem.option,
                "quantity": cartItem.quantity - 1
            });
        }
    }

    cartProductQuantityUp(cartItem) {
        console.log("ürün adet değeri artırılıy.");
        this.setState({
            cartModel: cartItem,
            isquantityup: true,
        });
        this.updateCart({
            "cart_id": cartItem.cart_id,
            "customer_id": cartItem.customer_id,
            "session_id": cartItem.session_id,
            "product_id": cartItem.product_id,
            "option": cartItem.option,
            "quantity": cartItem.quantity + 1
        });
    }

    renderDownButton(cartItem) {
        if (this.state.isquantitydown && this.state.cartModel.cart_id == cartItem.cart_id) {
            return (
                <View style={{
                    flex: 1,
                    justifyContent: 'center',
                    alignItems: 'center',
                    padding: 8,
                }}><ActivityIndicator animating={true} size='small' /></View>
            );
        }
        else {
            return (
                <Text
                    style={{
                        paddingTop: 13.5,
                        paddingBottom: 13.5,
                        paddingLeft: 15,
                        paddingRight: 15,
                    }}>
                    <FontAwesomeIcons name="minus" size={10} color="#000000" />
                </Text>
            );
        }
    }

    renderUpButton(cartItem) {
        if (this.state.isquantityup && this.state.cartModel.cart_id == cartItem.cart_id) {
            return (
                <View style={{
                    flex: 1,
                    justifyContent: 'center',
                    alignItems: 'center',
                    padding: 8,
                }}><ActivityIndicator animating={true} size='small' /></View>
            );
        }
        else {
            return (
                <Text
                    style={{
                        paddingTop: 13.5,
                        paddingBottom: 13.5,
                        paddingLeft: 15,
                        paddingRight: 15,
                    }}>
                    <FontAwesomeIcons name="plus" size={10} color="#000000" />
                </Text>
            );
        }
    }

    renderCartItem(cartItem) {
        return (
            <View
                style={[ShoppingCartStyles.shoppingcart, {
                    padding: 20,
                }]}>
                <View
                    style={{
                        padding: 5,
                    }}>
                    <Text
                        style={{
                            fontFamily: 'Poppins-Regular',
                            fontWeight: 'bold',
                            fontSize: 18,
                            color: '#6f6f6f',
                        }}>{cartItem.product.product_description_list[0].name}</Text>
                    <Text
                        style={{
                            position: 'absolute',
                            right: 5,
                            top: 5,
                            fontFamily: 'Poppins-Regular',
                            fontWeight: 'bold',
                            fontSize: 15,
                            color: '#9e7538',
                        }}>{this.getCartPrice(cartItem)} CHF</Text>
                </View>
                <View
                    style={{
                        padding: 5,
                    }}>
                    <Text
                        style={{
                            fontFamily: 'Poppins-Regular',
                            fontSize: 15,
                            color: '#6f6f6f',
                        }}>{cartItem.product.model}</Text>
                </View>
                <View
                    style={{
                        padding: 5,
                    }}>
                    {
                        cartItem.product_option_list.map((data, index) => {
                            return (
                                <Text
                                    style={{
                                        fontFamily: 'Poppins-Regular',
                                        fontSize: 15,
                                        color: '#6f6f6f',
                                    }}>{data.option_description.name}:
                                    <Text
                                        style={{
                                            fontFamily: 'Poppins-Regular',
                                            fontSize: 15,
                                            color: '#6f6f6f',
                                            textDecorationLine: data.option_id == 13 ? 'line-through' : 'none',
                                        }}>
                                        {
                                            data.product_option_value_list.map((prodOptVal, index) => {
                                                return (
                                                    prodOptVal.option_value_description_list[0].name + ((data.product_option_value_list.length == (index + 1)) ? "" : ",")
                                                )
                                            })
                                        }
                                    </Text>
                                </Text>
                            )
                        })
                    }
                </View>
                <View
                    style={{
                        flexDirection: 'row',
                        padding: 5,
                    }}>
                    <TouchableOpacity
                        onPress={() => {
                            console.log(cartItem.cart_id, cartItem.product.product_id, "sipariş ürün adet azaltma");
                            this.cartProductQuantityDown(cartItem);
                        }}
                        style={{
                            alignSelf: 'flex-start',
                            borderColor: '#CDCDCD',
                            borderWidth: 0.5,
                            borderTopLeftRadius: 12,
                            borderBottomLeftRadius: 12,
                        }}>
                        {
                            this.renderDownButton(cartItem)
                        }
                    </TouchableOpacity>
                    <View
                        style={{
                            alignSelf: 'flex-start',
                            borderColor: '#CDCDCD',
                            borderWidth: 0.5,
                        }}>
                        <Text
                            style={{
                                paddingTop: 5,
                                paddingBottom: 5,
                                paddingLeft: 15,
                                paddingRight: 15,
                                fontFamily: 'Poppins-Regular',
                                fontSize: 18,
                                fontWeight: 'bold',
                                color: '#397596',
                            }}>{cartItem.quantity}</Text>
                    </View>
                    <TouchableOpacity
                        onPress={() => {
                            console.log(cartItem.cart_id, cartItem.product.product_id, "sipariş ürün adet artırma");
                            this.cartProductQuantityUp(cartItem);
                        }}
                        style={{
                            alignSelf: 'flex-start',
                            borderColor: '#CDCDCD',
                            borderWidth: 0.5,
                            borderTopRightRadius: 12,
                            borderBottomRightRadius: 12,
                        }}>
                        {
                            this.renderUpButton(cartItem)
                        }
                    </TouchableOpacity>
                    <TouchableOpacity
                        style={{
                            flex: 1,
                            alignItems: 'flex-end',
                            paddingTop: 5,
                        }}
                        onPress={() => {
                            this.deleteCartFromList(cartItem);
                        }}>
                        <Text><FontAwesomeIcons name="trash" size={25} color="#397596"></FontAwesomeIcons></Text>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }

    renderShoppingCartList() {
        if (this.state.cartlist) {
            return (
                <View
                    style={{
                        padding: 15,
                    }}>
                    <FlatList
                        style={{
                        }}
                        data={this.state.cartlist}
                        renderItem={({ item }) => this.renderCartItem(item)}
                        keyExtractor={item => String(item.cart_id)}
                    />
                </View>
            );
        }
        else {
            if (this.state.refreshing) {
                return (
                    <View
                        style={{
                            flex: 1,
                            backgroundColor: '#FFFFFF',
                            justifyContent: 'center',
                            alignItems: 'center',
                            paddingHorizontal: 35,
                        }}>
                        <ActivityIndicator
                            animating={true}
                            size='small'
                        />
                    </View>
                );
            }
            else {
                return (
                    <View>
                        <Text>No data!!!</Text>
                    </View>
                );
            }
        }
    }

    refreshCartList() {
        this.setState({
            refreshing: true,
        });
        wait(2000).then(() => {
            this.getCartList();
        });
    }

    componentDidMount() {
        this.getCartList();
    }


    render() {
        return (
            <View
                style={{
                    backgroundColor: '#E9EAEE',
                    flex: 1,
                }}>
                <StatusBar />
                <Header
                    containerStyle={HeaderStyles.container}
                    leftComponent={
                        <TouchableOpacity
                            onPress={() => { this.props.navigation.goBack(null) }}>
                            <FontAwesomeIcons name="chevron-left" style={HeaderStyles.title} />
                        </TouchableOpacity>
                    }
                    centerComponent={<Text style={HeaderStyles.title}>Shopping Cart</Text>}
                    rightComponent={
                        <TouchableOpacity
                            onPress={() => { this.props.navigation.dispatch(DrawerActions.openDrawer()) }}>
                            <FontAwesomeIcons name="bars" style={HeaderStyles.title} />
                        </TouchableOpacity>
                    }
                />
                <ScrollView
                    refreshControl={<RefreshControl refreshing={this.state.refreshing} onRefresh={() => this.refreshCartList()}></RefreshControl>}
                    style={{
                        flex: 1
                    }}>
                    {
                        this.renderShoppingCartList()
                    }
                </ScrollView>
                <View
                    style={{
                        flexDirection: 'row',
                        marginBottom: 50,
                    }}>
                    <View
                        style={{
                            flex: 1,
                            backgroundColor: '#FFFFFF',
                            alignItems: 'center',
                            justifyContent: 'center',
                            width: '100%',
                            paddingBottom: 15,
                            paddingTop: 15,
                        }}>
                        <Text
                            style={{
                                color: '#9e7538',
                                fontSize: 19,
                                fontFamily: 'Poppins-Regular',
                                fontWeight: 'bold',
                            }}>{this.getTotalCartPrice()} CHF</Text>
                    </View>
                    <View
                        style={{
                            flex: 1,
                            backgroundColor: '#6ace4f',
                            alignItems: 'center',
                            justifyContent: 'center',
                        }}>
                        <TouchableOpacity
                            onPress={() => {
                                this.props.navigation.navigate('CompleteOrder', {
                                    cartlist: this.state.cartlist
                                });
                            }}
                            style={{
                                flex: 1,
                                alignItems: 'center',
                                justifyContent: 'center',
                                width: '100%',
                                paddingBottom: 15,
                                paddingTop: 15,
                            }}>
                            <Text
                                style={{
                                    color: '#FFFFFF',
                                    fontSize: 15,
                                    fontFamily: 'Poppins-Regular',
                                    padding: 15,
                                }}>Complete Shopping</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        );
    }
}

function wait(timeout) {
    return new Promise(resolve => {
        setTimeout(resolve, timeout);
    });
}

const mapStateToProps = state => ({
    setting: state.setting,
    auth: state.auth,
    language: state.language,
    customer: state.customer,
});

const mapDispatchToProps = dispatch => ({
    onLogin: (customer) => {
        console.log(customer, "onlogin");
        dispatch({
            type: "CUSTOMER",
            payload: customer
        });
    },
});

export default connect(mapStateToProps, mapDispatchToProps)(Index);