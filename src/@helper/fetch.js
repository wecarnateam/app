import base64 from 'react-native-base64';

export const API = 'http://api.wecarna.com/api'

export function DELETE (path, state) {
   const authorization = (state.authorization && state.authorization.type=="basic") ? { 'Authorization': 'Basic ' + base64.encode(state.authorization.username + ":" + state.authorization.password) } : {}
   const url = new URL(path, state.api ? state.api : API)
   if (state.data!=null) {
      Object.keys(state.data).forEach(key => {
         url.searchParams.append(key, state.data[key])
      })
   }
   const config = {
      method: 'DELETE',
      headers: {
         'Accept': 'application/json',
         'Content-Type': 'application/json',
         ...authorization,
         ...state.headers,
      },
      ...state.config
   }
   state.log && console.log(url.href, config)
   return fetch(url.href, config)
}

export function GET (path, state) {
   const authorization = (state.authorization && state.authorization.type === "basic") ? { 'Authorization': 'Basic ' + base64.encode(state.authorization.username + ":" + state.authorization.password) } : {}
   const url = new URL(path, state.api ? state.api : API)
   if (state.data!=null) {
      Object.keys(state.data).forEach(key => {
         url.searchParams.append(key, state.data[key])
      })
   }
   const config = {
      method: 'GET',
      headers: {
         'Accept': 'application/json',
         'Content-Type': 'application/json',
         ...authorization,
         ...state.headers,
      },
      ...state.config
   }
   state.log && console.log('durum', url.href, config)
   return fetch(url.href, config)
}

export function POST (path, state) {
   const authorization = (state.authorization && state.authorization.type !== undefined && state.authorization.type === "basic") ? { 'Authorization': 'Basic ' + base64.encode(state.authorization.username + ":" + state.authorization.password) } : {}
   const config = {
      method: 'POST',
      headers: {
         'Accept': 'application/json',
         'Content-Type': 'application/json',
         ...state.headers,
      },
      body: (state.headers && state.headers['Content-Type'] && state.headers['Content-Type']==='multipart/form-data') ? state.data : JSON.stringify({
         ...state.data
      }),
   }
   state.log && console.log('post', 'url', `${state.api ? state.api : API}${path}`, 'config', config)
   return fetch(`${state.api ? state.api : API}${path}`, config)
}

export function PUT (path, state) {
   const authorization = (state.authorization && state.authorization.type=="basic") ? { 'Authorization': 'Basic ' + base64.encode(state.authorization.username + ":" + state.authorization.password) } : {}
   const config = {
      method: 'PUT',
      headers: {
         'Accept': 'application/json',
         'Content-Type': 'application/json',
         ...authorization,
         ...state.headers,
      },
      body: (state.headers && state.headers['Content-Type'] && state.headers['Content-Type']==='multipart/form-data') ? state.data : JSON.stringify({
         ...state.data
      }),
   }
   state.log && console.log(`${state.api ? state.api : API}${path}`, config)
   return fetch(`${state.api ? state.api : API}${path}`, config)
}
