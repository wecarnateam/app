import React from 'react'
import { Dimensions, Platform, StatusBar, } from 'react-native'

export const width = Dimensions.get('window').width
export const height = Dimensions.get('window').height
export const scale = Dimensions.get('window').scale

// https://github.com/ovr/react-native-status-bar-height
const X_WIDTH = 375;
const X_HEIGHT = 812;

const XSMAX_WIDTH = 414;
const XSMAX_HEIGHT = 896;

const { height: W_HEIGHT, width: W_WIDTH } = Dimensions.get('window');

let isIPhoneX = false;

if (Platform.OS === 'ios' && !Platform.isPad && !Platform.isTVOS) {
    isIPhoneX = W_WIDTH === X_WIDTH && W_HEIGHT === X_HEIGHT || W_WIDTH === XSMAX_WIDTH && W_HEIGHT === XSMAX_HEIGHT;
}

export function getStatusBarHeight(skipAndroid) {
   return Platform.select({
      ios: isIPhoneX ? 44 : 20,
      android: skipAndroid ? 0 : StatusBar.currentHeight,
      default: 0
   })
}

export function getBottomBarHeight() {
   return Platform.select({
      ios: isIPhoneX ? 34 : 0,
      default: 0
   })
}

// Auto Size
export const autoHeight = (size, width) => {
   aspectRatio = size.height / size.width
   outputHeight = width * aspectRatio
   outputWidth = outputHeight / aspectRatio
   return {
      width: Math.round(outputWidth),
      height: Math.round(outputHeight),
   }
}

export const autoWidth = (size, height) => {
   aspectRatio = size.width / size.height
   outputWidth = height * aspectRatio
   outputHeight = outputWidth / aspectRatio
   return {
      width: Math.round(outputWidth),
      height: Math.round(outputHeight),
   }
}
