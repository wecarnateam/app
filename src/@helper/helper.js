import React from 'react'
import { Dimensions, Platform, StatusBar, Linking } from 'react-native';
import base64 from 'react-native-base64';

// Fetch
export function DELETE(path, option) {
    const url = new URL(path, option.setting.api)
    if (option.data != null) {
        if (!option.data.language) url.searchParams.append('language', option.setting.language)
        Object.keys(option.data).forEach(key => {
            url.searchParams.append(key, option.data[key])
        })
    }
    const config = {
        method: 'DELETE',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            ...option.headers,
        },
        ...option.config
    }

    option.log && console.log(url.href, config)
    return fetch(url.href, config)
}

export function GET(path, option) {
    const authorization = (option.authorization.type=="basic") ? { 'Authorization': 'Basic ' + base64.encode(option.authorization.username + ":" + option.authorization.password) } : {}
    const url = new URL(path, option.setting.api)
    if (option.data != null) {
        if (!option.data.language) url.searchParams.append('language', option.setting.language)
        Object.keys(option.data).forEach(key => {
            url.searchParams.append(key, option.data[key])
        })
    }
    const config = {
        method: 'GET',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            ...authorization,
            ...option.headers,
        },
        ...option.config
    }
    option.log && console.log(url.href, config)
    return fetch(url.href, config)
}

export function POST(path, option) {
    const config = {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            ...option.headers,
        },
        body: JSON.stringify({
            device: option.setting.device,
            language: option.setting.language,
            ...option.data
        }),
    }
    option.log && console.log(`${option.setting.api}${path}`, config)
    return fetch(`${option.setting.api}${path}`, config)
}

export function PUT(path, option) {
    const authorization = option.user.token ? { 'Authorization': `Bearer ${option.user.token}` } : {}
    const config = {
        method: 'PUT',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            ...authorization,
            ...option.headers,
        },
        body: JSON.stringify({
            device: option.user.device,
            language: option.setting.language,
            ...option.data
        }),
    }
    option.log && console.log(`${option.setting.api}${path}`, config)
    return fetch(`${option.setting.api}${path}`, config)
}