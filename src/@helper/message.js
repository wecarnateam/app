import React from 'react'
import { Linking } from 'react-native'
import { showMessage as Message } from 'react-native-flash-message'

export function showMessage(data) {
   const config = {}

   if (typeof data === 'object') {
      data.icon && (config.icon = data.icon)
      data.type && (config.type = data.type)
      data.duration && (config.duration = data.duration)
      data.backgroundColor && (config.backgroundColor = data.backgroundColor)
      data.hideStatusBar && (config.hideStatusBar = data.hideStatusBar)
      data.color && (config.color = data.color)
      data.url && (config.onPress = () => {
         Linking.canOpenURL(data.url).then(supported => {
            return supported ? Linking.openURL(data.url) : false
         })
      })
      data.onPress && (config.onPress = data.onPress)
      data.title && (config.message = data.title)
      data.description && (config.description = data.description)

      Message({
         ...data,
         ...config
      })
   } else {
      Message({ message: data })
   }

}
