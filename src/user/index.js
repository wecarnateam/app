import React, { PureComponent, Component } from 'react';

import {
	Text,
	View,
	TextInput,
	Alert,
	Dimensions,
	StyleSheet,
	TouchableOpacity,
	ScrollView,
	FlatList,
	ActivityIndicator,
	Icon,
	AsyncStorage
} from 'react-native';
import { Button, ButtonGroup, ListItem, Header, Image, } from 'react-native-elements';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import { POST, GET } from '../@helper/helper';
import { DrawerActions } from 'react-navigation-drawer';
import HeaderStyles from '../@assets/header';
import StatusBar from '../component/statusbar';

class LoginPage extends Component {

	constructor(props) {
		super(props); // super arguman geçmenizi sağlar eğer constructor kullanmak isterseniz kullanmak zorunlu oluyor.

		this.state = { // burası bind da kullandığım değerler
			mail: null,
			password: null,
			submiting: false,
			mailrequired: null,
			passwordrequired: null,
			...props.navigation.state.params
		};
	}

	getLogin() {
		if (this.state.mailrequired !== null && this.state.passwordrequired !== null) {
			if (this.state.mailrequired && this.state.passwordrequired) {
				GET('/login', {
					setting: this.props.setting,
					authorization: this.props.auth,
				})
					.then((response) => response.json())
					.then((responseJson) => {
						//console.log(responseJson);
						POST('/customerinfo', {
							setting: this.props.setting,
							headers: {
								'x-access-token': responseJson.token
							},
							data: {
								email: this.state.mail,
								password: this.state.password
							},
						})
							.then((response) => response.json())
							.then((responseJson) => {
								//console.log(responseJson.result, 'kullanıcı giriş yaptıııııııııı.');
								this.setState({
									submiting: false
								});

								AsyncStorage.setItem('@customer', JSON.stringify(responseJson.result));
								this.props.onLogin(responseJson.result);
								this.props.navigation.navigate("Home");
							})
							.catch((error) => {
								console.log(error, '1');
								this.setState({
									submiting: false
								});
							});
					})
					.catch((error) => {
						console.log(error, '2');
						this.setState({
							submiting: false
						});
					});
			}
			else {
				this.setState({
					submiting: false
				});
			}
		}
		else {
			this.setState({
				submiting: false
			});
		}
	}

	checkMail = mail => {
		if (this.state.mailrequired !== null) {
			let reg = /^([a-z\+\-]+)+(@)+([a-z\+\-]+)+(\.)+([a-z\+\-]{2,3})$/
			if (reg.test(mail) === false) {
				this.setState({
					mailrequired: false
				});
			}
			else {
				this.setState({
					mailrequired: true
				});
			}
		} else {
			this.setState({
				mailrequired: false,
			});
		}
	}

	checkPassword = password => {
		if (this.state.passwordrequired !== null) {
			if (password.length < 3) {
				this.setState({
					passwordrequired: false
				});
			}
			else {
				this.setState({
					passwordrequired: true
				});
			}
		}
		else {
			this.setState({
				passwordrequired: false
			});
		}
	}

	getCustomerAsync = async () => {
		try {
			const value = await AsyncStorage.getItem('@customer');
			if (value !== null) {
				// We have data!!
				// console.log(this.state.customer, 'adsdsfsdfsfs');
				this.setState({
					customer: value
				});
				// console.log(value);
			}
		} catch (error) {
			// Error retrieving data
		}
	};

	componentDidMount() {
	}

	render() {
		const { navigate, goBack } = this.props.navigation;
		return (
			<View
				style={{
					flex: 1,
				}}>
				<StatusBar />
				<Header
					containerStyle={HeaderStyles.container}
					leftComponent={{
						icon: 'close', type: 'material', color: '#F23D3D', onPress: () => {
							//console.log(this.props.navigation, "asdsdasdadasd");
							this.props.navigation.navigate("Home");
						}
					}
					}
					centerComponent={{ text: 'Product Detail', style: { color: '#fff' } }}
					rightComponent={{ icon: 'home', color: '#fff' }} />

				<View
					style={{
						flex: 1,
						padding: 15
					}}>
					<View style={[css.viewInput, this.state.mail && (!this.state.mailrequired ? css.invalid : '')]} >
						<TextInput
							style={{
								flex: 1
							}}
							value={this.state.mail}
							onChangeText={(value) => {
								this.setState({
									mail: value
								}, () => this.checkMail(value));
							}}
							autoCapitalize='none'
							placeholder="Kullanıcı adı" />
					</View>
					<View style={[css.viewInput, this.state.password && (!this.state.passwordrequired ? css.invalid : '')]} >
						<TextInput
							style={{
								flex: 1
							}}
							allowFontScaling={false}
							autoCapitalize='none'
							autoCompleteType='off'
							autoCorrect={false}
							clearButtonMode='while-editing'
							clearTextOnFocus={false}
							keyboardAppearance='default'
							keyboardType='default'
							spellCheck={false}
							secureTextEntry={true}
							type='password'
							onChangeText={(value) => {
								this.setState({
									password: value
								});
								this.checkPassword(value);
							}}
							value={this.state.password}
							placeholder="Şifre" />
					</View>
					<View
						style={{
						}}>
						<TouchableOpacity
							style={{
								alignItems: "center",
								padding: 15,
								backgroundColor: "#4285F4",
								borderRadius: 4,
							}}
							onPress={() => {
								this.setState({
									submiting: true,
								});
								this.getLogin();
							}}>
							{!this.state.submiting && <Text
								style={{
									fontFamily: 'Poppins-Regular',
									fontSize: 19,
									paddingRight: 10,
									color: '#FFFFFF',
								}}>Login</Text>
							}
							{this.state.submiting && <ActivityIndicator animating={true} size='small' color='#FFFFFF' style={{ paddingRight: 10, }} />}
						</TouchableOpacity>
					</View>
				</View>

			</View>
		);
	}
}

const css = {
	viewInput: {
		flexDirection: 'row',
		height: 50,
		marginBottom: 10,
		backgroundColor: '#FFFFFF',
		borderRadius: 4,
		paddingLeft: 5,
		paddingRight: 5,
		borderWidth: 1,
		borderColor: '#EFEFEF'
	},
	invalid: {
		borderColor: '#d9534f',
	},
	validated: {
		borderColor: '#EFEFEF'
	}
}

const mapStatetoProps = state => ({
	setting: state.setting,
	auth: state.auth,
	customer: state.customer,
	language: state.language,
});

const mapDispatchToProps = dispatch => ({
	onLogin: (customer) => {
		console.log(customer, "onlogin");
		dispatch({
			type: "CUSTOMER",
			payload: customer
		});
	},
});

export default connect(mapStatetoProps, mapDispatchToProps)(LoginPage)