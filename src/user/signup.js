import React, { PureComponent } from 'react';

import {
	Text,
	View,
	TextInput,
  Alert,
  Dimensions,
  StyleSheet,
  TouchableOpacity,
  ScrollView,
  FlatList
} from 'react-native';
import { Button, ButtonGroup, ListItem, Header, Image, } from 'react-native-elements';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { POST, GET } from '../@helper/helper';

class signUp extends PureComponent {
  constructor(props) {
      super(props);
      this.state = {
        firstname: '',
        lastname: '',
        telephone: '',
        email: '',
        password: '',
        ...props.navigation.state.params,
      }
  }

	// giriş isteği atacağımız ve yükleceğimiz fonksiyon
	/*goLogin() {
		var name = this.state.userName;
		var pass = this.state.userPassword;
		var present = this;

		// react native ajax komutu
		fetch('http:/192.168.1.100/index.php', { // extralar
			method: 'POST',
			headers: {
			    'Content-Type': 'application/x-www-form-urlencoded',
			},
			body: serializeKey({ // değerleri serialize ediyoruz
				userName: name,
				userPassword: pass
			})
		})
	    .then((res) => res.json()) // gelen datayı parse ediyoruz
	    .then((res) => {
	    	if (res.result != -1)
	    		present.props.navigator.push({
	    			id: 'Main',
					name: 'Main'
	    		})
	    	else
	    		Alert.alert("Kullanıcı doğrulanamadı");
	    })
	    .catch((error) => {
			Alert.alert("data", "Sunucuya bağlanırken bir hata oluştu" + error);
	    });
  }*/
  
  getCustomerCreate() {
    GET('/login', {
        setting: this.props.setting,
        authorization: this.props.auth,
    })
        .then((response) => response.json())
        .then((responseJson) => {
            console.log(responseJson);
            POST('/customer', {
                setting: this.props.setting,
                headers: {
                    'x-access-token': responseJson.token
                },
                data: {
                  id: 0,
                  firstname: this.state.firstname,
                  lastname:  this.state.lastname,
                  telephone:  this.state.telephone,
                  email:  this.state.email,
                  password:  this.state.password
                },
            })
                .then((response) => response.json())
                .then((responseJson) => {
                    debugger;
                    console.log(responseJson);
                })
                .catch((error) => {
                    console.log(error);
                });
        })
        .catch((error) => {
            console.log(error);
        });
}

	render() {
		return (
			<View
				style={{
					flex: 1,
					flexDirection: 'column',
			        justifyContent: 'center',
			        padding: 15
				}}>
				<View>
					<TextInput
						style={{
							height: 50
						}}
						value={this.state.firstname}
						onChangeText={(value) => this.setState({firstname: value})}
						placeholder="First Name"/>
				</View>
        <View>
					<TextInput
						style={{
							height: 50
						}}
						value={this.state.lastname}
						onChangeText={(value) => this.setState({lastname: value})}
						placeholder="Last Name"/>
				</View>
        <View>
					<TextInput
						style={{
							height: 50
						}}
						value={this.state.telephone}
						onChangeText={(value) => this.setState({telephone: value})}
						placeholder="Telephone"/>
				</View>
        <View>
					<TextInput
						style={{
							height: 50
						}}
						value={this.state.email}
						onChangeText={(value) => this.setState({email: value})}
						placeholder="E-mail"/>
				</View>
				<View>
					<TextInput
						style={{
							height: 50
						}}
						onChangeText={(value) => this.setState({password: value})}
						value={this.state.password}
						placeholder="Şifre"/>
				</View>
        
				<View
					style={{
						height: 50
					}}>
					<Button
					  	title="Giriş" // butonun yazısı
					  	color="#4285f4" // arkaplan rengi
					  	onPress={this.getCustomerCreate.bind(this)} /* butona tıklandığında tetiklenecek fonksiyon*/ />
				</View>
			</View>
		);
	}
}



const mapStatetoProps = state => ({
  setting: state.setting,
  auth: state.auth,
  language: state.language,
});

export default connect(mapStatetoProps)(signUp)