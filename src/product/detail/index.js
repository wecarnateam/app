import React, { PureComponent, Component  } from 'react';
import { FlatList, View, Text, TouchableOpacity, ScrollView, Dimensions } from 'react-native';
import { connect } from 'react-redux';
import { Button, ButtonGroup, ListItem, Header, Image } from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';
import SectionedMultiSelect from 'react-native-sectioned-multi-select';

import { POST, GET } from '../../@helper/helper';
import HeaderStyles from '../../@assets/header';
import ProductDetailStyles from '../../@assets/product/detail';

const screenWidthHeight = {
    width: Dimensions.get('screen').width,
    height: Dimensions.get('screen').height,
}



class Index extends PureComponent {

 

    constructor(props) {
        super(props);
        this.state = {
            productoptions: null,
            productQuantity: 1,
            productTotalPrice: 1,
            productPriceOrj : 0,
            onSelectedTotalPrice:0,
            selectedvalue:[],
            selectedItems: [],
            option: {},
            ...props.navigation.state.params,
        }

     
        
    
    }


    getCartCreate() {
      GET('/login', {
            setting: this.props.setting,
            authorization: this.props.auth,
        })
            .then((response) => response.json())
            .then((responseJson) => {
                console.log(responseJson);
                POST('/cart', {
                    setting: this.props.setting,
                    headers: {
                        'x-access-token': responseJson.token
                    },
                    data: {
                      cart_id: 0,
                      customer_id: this.props.customer.customer.customer.customer_id,
                      session_id:  '',
                      product_id:  this.state.productid,
                      option:  this.state.option,
                      quantity:  this.state.productQuantity
                    },
                })
                    .then((response) => response.json())
                    .then((responseJson) => {
                        debugger;
                        console.log(responseJson);
                        this.props.navigation.navigate("ShoppingCart");
                    })
                    .catch((error) => {
                        console.log(error);
                    });
            })
            .catch((error) => {
                console.log(error);
            });
    }

    getProductDetail() {
        GET('/login', {
            setting: this.props.setting,
            authorization: this.props.auth,
        })
            .then((response) => response.json())
            .then((responseJson) => {
                //console.log(responseJson);
                POST('/productoptionsbyproduct', {
                    setting: this.props.setting,
                    headers: {
                        'x-access-token': responseJson.token
                    },
                    data: {
                        productid: this.state.productid,
                    },
                })
                    .then((response) => response.json())
                    .then((responseJson) => {
                        //console.log(responseJson, 'geldiiiiii');
                        responseJson.productoptions.map((produtOpt, index) => {
                            produtOpt.product_option_value_list.map((optionValue, index) => {
                                optionValue.isselected = false;
                            });
                        });
                        this.setState({
                            ...responseJson
                        });
                    })
                    .catch((error) => {
                        console.log(error);
                    });
            })
            .catch((error) => {
                console.log(error);
            });
    }


    checkOptionConvert() {
        this.setState({option: {} });
        let proOptId = 0
        let optionList =  {};
        this.state.productoptions.map((productOption, index) => {
        
            let optIdList = []
            console.log(productOption.option.type, 'type');
            
            productOption.product_option_value_list.map((data, index) => {
                if(data.isselected==true){
                    if(productOption.option.type=='radio'){
                        optIdList = data.product_option_value_id
                    }else{
                         optIdList.push(data.product_option_value_id)
                    }
                       
                    console.log(optIdList, "optionCheck")
                }
                    
            });    
            
            proOptId = productOption.product_option_id

           // console.log(proOptId+'='+productOption.option_description.name+'='+optIdList, "optionCheck2")

            optionList[proOptId]=optIdList

            //console.log(optionCheckDetail, "optIdList")
            

        });  

        

       
        
        this.setState({option:optionList});     


        console.log(this.state.option, "optionCheck")
    }
 


    uncheckProductOption(productOptionValueId,productOptionPrice){
        //console.log("uncheckProductOption çalıştı");
        this.state.productoptions.map((productOption, index) => {
            productOption.product_option_value_list.map((data, index) => {
                if(data.product_option_value_id == productOptionValueId)
                    data.isselected = false;
            });            
    });
    this.setState({
        ...this.state.productoptions,
        ...this.state.productPrice = (parseFloat(this.state.productPrice)- parseFloat(productOptionPrice)).toFixed(2)
    });

    }
    checkProductOption(productOptionValueId,productOptionPrice){
        //console.log("checkProductOption çalıştı");
        this.state.productoptions.map((productOption, index) => {
                productOption.product_option_value_list.map((data, index) => {
                    if(data.product_option_value_id == productOptionValueId)
                        data.isselected = true;
                });            
        });
        this.setState({
            ...this.state.productoptions,
            ...this.state.productPrice = (parseFloat(this.state.productPrice) + parseFloat(productOptionPrice)).toFixed(2)
        });   
    }

    radioCheckProductOption(productOptionValueId,productOptionId,productOptionPrice){
        
      
        this.productOptionSelectPrice = 0;
        this.state.productoptions.map((productOption, index) => {
            productOption.product_option_value_list.map((data, index) => {

                if(data.option_id == productOptionId && data.product_option_value_id != productOptionValueId && data.isselected){
                    this.productOptionSelectPrice = data.price;
                    data.isselected = false;
                }

                if(data.product_option_value_id == productOptionValueId)
                    data.isselected = true;
                   
            });    
                 
        });

         
        this.setState({
            ...this.state.productPrice = (parseFloat(this.state.productPrice) - parseFloat(this.productOptionSelectPrice)).toFixed(2),
            ...this.state.productoptions,
            ...this.state.productPrice = (parseFloat(this.state.productPrice) + parseFloat(productOptionPrice)).toFixed(2)
        });  

        //console.log(this.state.productoptions,'productoptions Radio');
    }

    selectItem(productOpt,optionType){
        //console.log(productOpt, "sdfdsfdsfsfsfd");
        /*if(productOpt.type == 0 ){
            this.uncheckProductOption(productOpt.product_option_id);
        }*/
        if(optionType=='radio'){
            this.radioCheckProductOption(productOpt.product_option_value_id,productOpt.option_id,productOpt.price);
        }else{
            if(productOpt.isselected==true){
                this.uncheckProductOption(productOpt.product_option_value_id,productOpt.price);
            }else{
                this.checkProductOption(productOpt.product_option_value_id,productOpt.price);
            }
        }

        this.checkOptionConvert()

    }

    componentDidMount() {
        this.getProductDetail();
    }

    onselectItemIsSelected(productOptionValueId){
        this.state.productoptions.map((productOption, index) => {
            productOption.product_option_value_list.map((data, index) => {
                if(data.product_option_value_id == productOptionValueId)
                    data.isselected = true;
            });            
        });
        this.setState({
            ...this.state.productoptions,
        });   
    }
    
    onSelectedItemsChange = (selectedItems) =>{
        console.log(selectedItems, "SelectItems");
        this.setState({ selectedItems });

        this.state.productoptions.map((productOption, index) => {
            productOption.product_option_value_list.map((data, index) => {
                if(selectedItems.includes(data.product_option_value_id))
                    data.isselected = true;
            });            
        });
        this.setState({
            ...this.state.productoptions,
        });   

        this.checkOptionConvert()

      };


      onSelectedItemsChangeData = (selectedItemsData) =>{
            
            this.state.onSelectedTotalPrice = 0;
           
            selectedItemsData.map((selectedItem) => {
               
                this.state.onSelectedTotalPrice =  (parseFloat(this.state.onSelectedTotalPrice) + parseFloat(selectedItem.price)).toFixed(2);
            });
            
           
            
        
           // console.log(this.state.optionExtra,'deneme deneme');

      };


      renderDownButton() {
        
            return (
                <Text
                    style={{
                        paddingTop: 13.5,
                        paddingBottom: 13.5,
                        paddingLeft: 15,
                        paddingRight: 15,
                    }}>
                    <Icon name="minus" size={10} color="#000000" />
                </Text>
            );
        
    }

    renderUpButton() {
     
            return (
                <Text
                    style={{
                        paddingTop: 13.5,
                        paddingBottom: 13.5,
                        paddingLeft: 15,
                        paddingRight: 15,
                    }}>
                    <Icon name="plus" size={10} color="#000000" />
                </Text>
            );
       
    }

    cartProductQuantityDown(productQuantity) {
        if (productQuantity > 1) {
            this.setState({
                productQuantity: productQuantity-1,
         
            });
            
        }
    }

    cartProductQuantityUp(productQuantity) {
        this.setState({
            productQuantity: productQuantity+1,
        });
    }


    renderProductDetail() {
       // console.log(this.state, 'propductoptions');
        
        const { selectedItems } = this.state;
        if (this.state.productoptions === null) {
            return (
                <View><Text>Nooooooooo!!!burasi</Text></View>
            )
        }
        else if (Array.isArray(this.state.productoptions) && this.state.productoptions.length > 0) {

            const groupList = this.state.productoptions.map((data, index) => {
                if(data.option_id=='14'){
                    return  (
                    <View>
                    <Text style={ProductDetailStyles.OptionLabel}>{data.option_description.name}</Text>
    
                                <View   style={ProductDetailStyles.MultiSelect} >
                                
                                <SectionedMultiSelect
          items={data.product_option_value_list}
          uniqueKey="product_option_value_id"
          displayKey="option_value_name"
          subKey="children"
          selectText="Choose some things..."
          showDropDowns={true}
          readOnlyHeadings={false}
          onSelectedItemsChange={this.onSelectedItemsChange}
          onSelectedItemObjectsChange={this.onSelectedItemsChangeData}
          selectedItems={this.state.selectedItems}
          ref={SectionedMultiSelect => this.SectionedMultiSelect = SectionedMultiSelect}
        />
                               
           <View>
        </View>
    
                
                                  
                                </View>
                       
                        </View>
                    )

                }else{
                return  (
                    <View>
                <Text style={ProductDetailStyles.OptionLabel}>{data.option_description.name}</Text>

                <View   style={ProductDetailStyles.OptionGroup} >
                    <FlatList
                        data={data.product_option_value_list}
                        contentContainerStyle={{
        flexDirection: 'row',
        flexWrap: 'wrap'}}
                        renderItem={({ item }) => (
                            <View         >
                            
<TouchableOpacity onPress={() =>{
    if(item.isselected==true && data.option.type=='radio'){
        
    }else{
        this.selectItem(item,data.option.type);
    }
   
    //console.log(item, "dsfsdfsfsdf");
this.setState({stateExample: item.product_option_value_id});
}} >
   <View  style={item.isselected==true ? ProductDetailStyles.OptionItemActive:ProductDetailStyles.OptionItem}>
       <Text style={ data.option_id=='13' && item.isselected==true ? ProductDetailStyles.OptionLabelOverline:'' } >{item.option_value_description_list[0].name}</Text>
   </View>
</TouchableOpacity>

            
                              
                            </View>
                        )}
                        keyExtractor={item => String(item.product_option_value_id)}
                    />
                    </View>
</View>
                )
            }
            });

            return (
                <View>
                    <View style={ProductDetailStyles.ImageFrame} >
                <Image
                                    source={{ uri: this.state.productImage }}
                                    style={{
                                        width: screenWidthHeight.width,
                                        height: 150,
                                    }} />
                                    <Text style={ProductDetailStyles.ImageFramePrice}>{(parseFloat(this.state.productPrice)+parseFloat(this.state.onSelectedTotalPrice)).toFixed(2)} CHF</Text>

                                    <View
                    style={ProductDetailStyles.ImageFrameQuantity}>
                    <TouchableOpacity
                        onPress={() => {
                            this.cartProductQuantityDown(this.state.productQuantity);
                        }}
                        style={{
                            alignSelf: 'flex-start',
                            borderColor: '#CDCDCD',
                            borderWidth: 0.5,
                            borderTopLeftRadius: 12,
                            borderBottomLeftRadius: 12,
                        }}>
                        {
                            this.renderDownButton(this.state.productQuantity)
                        }
                    </TouchableOpacity>
                    <View
                        style={{
                            alignSelf: 'flex-start',
                            borderColor: '#CDCDCD',
                            borderWidth: 0.5,
                        }}>
                        <Text
                            style={{
                                paddingTop: 5,
                                paddingBottom: 5,
                                paddingLeft: 15,
                                paddingRight: 15,
                                fontFamily: 'Poppins-Regular',
                                fontSize: 18,
                                fontWeight: 'bold',
                                color: '#397596',
                            }}>{this.state.productQuantity}</Text>
                    </View>
                    <TouchableOpacity
                        onPress={() => {
                            this.cartProductQuantityUp(this.state.productQuantity);
                        }}
                        style={{
                            alignSelf: 'flex-start',
                            borderColor: '#CDCDCD',
                            borderWidth: 0.5,
                            borderTopRightRadius: 12,
                            borderBottomRightRadius: 12,
                        }}>
                        {
                            this.renderUpButton(this.state.productQuantity)
                        }
                    </TouchableOpacity>
                    
                </View>

                                    </View>
                <View>{groupList}</View>
                </View>
              )
         /* return (
                <View>
                    <Image
                                        source={{ uri: this.state.productImage }}
                                        style={{
                                            width: screenWidthHeight.width,
                                            height: 150,
                                        }} />
                    <FlatList
                        data={this.state.productoptions}
                        renderItem={({ item }) => (
                            <View
                                style={{
                                    borderRadius: 25,
                                    backgroundColor: '#cccccc',
                                    margin: 5,
                                    paddingLeft: 15,
                                    paddingRight: 15,
                                }}>
                                <Text>{item.product_option_id}</Text>
                            </View>
                        )}
                        keyExtractor={item => String(item.product_option_id)}
                    />
                </View>
            )*/
        }
        else {
            return (
                <View><Text>No data!!!</Text></View>
            )
        }
    }

    render() {
        return (
            <View
            style={{
                backgroundColor: '#E9EAEF',
                flex: 1,
            }}>
                 
                <Header
                    containerStyle={HeaderStyles.container}
                    leftComponent={
                        <TouchableOpacity
                          onPress={() => {this.props.navigation.goBack(null)}}>
                          <Icon name="chevron-left"  style={HeaderStyles.title} />
                        </TouchableOpacity>
                      }
                    centerComponent={<Text style={HeaderStyles.title}>{this.state.productName}</Text>}
                    rightComponent={
                        <TouchableOpacity
                          onPress={() => { this.props.navigation.dispatch(DrawerActions.openDrawer())}}>
                          <Icon name="bars"  style={HeaderStyles.title} />
                        </TouchableOpacity>
                      }
                    />
                <ScrollView>
                    
                {
                    this.renderProductDetail()
                }
                </ScrollView>
                <View>
                <TouchableOpacity style={ProductDetailStyles.CartButton} onPress={() =>{ this.checkOptionConvert(), this.getCartCreate()  }} >
       <Text style={ProductDetailStyles.CartButtonText}>Sepete Ekle</Text>
</TouchableOpacity>
                </View>
            </View>
        );
    }
}

const mapStateToProps = state => ({
    setting: state.setting,
    auth: state.auth,
    customer: state.customer,
});

export default connect(mapStateToProps)(Index);