import React, { PureComponent } from 'react';
import { FlatList, View, Text, TouchableOpacity, ScrollView, ListView, Dimensions } from 'react-native';
import { connect } from 'react-redux';
import { Button, ButtonGroup, ListItem, Header, Image, } from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';
import { DrawerActions } from 'react-navigation-drawer';

import { POST, GET } from '../@helper/helper';
import HeaderStyles from '../@assets/header';
import ProductStyles from '../@assets/product';
import StatusBar from '../component/statusbar';

import { WEB_PAGE_URL, CATEGORY_LIST } from '../static/variables';

const screenWidthHeight = {
    width: Dimensions.get('screen').width,
    height: Dimensions.get('screen').height,
}

class Index extends PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            categorylist: null,
            ...props.navigation.state.params,
        }
        console.log(CATEGORY_LIST, 'category list');
    }

    getCategoryList(parentId) {
        GET('/login', {
            setting: this.props.setting,
            authorization: this.props.auth,
        })
            .then((response) => response.json())
            .then((responseJson) => {
                console.log(responseJson);
                POST('/categories', {
                    setting: this.props.setting,
                    headers: {
                        'x-access-token': responseJson.token
                    },
                    data: {
                        parentid: parentId,
                    },
                })
                    .then((response) => response.json())
                    .then((responseJson) => {
                        this.setState({ categorylist: responseJson.categories });
                        console.log(responseJson);
                    })
                    .catch((error) => {
                        console.log(error);
                    });
            })
            .catch((error) => {
                console.log(error);
            });
    }

    getProductList(categoryid) {
        GET('/login', {
            setting: this.props.setting,
            authorization: this.props.auth,
        })
            .then((response) => response.json())
            .then((responseJson) => {
                console.log(responseJson);
                POST('/productsbycategory', {
                    setting: this.props.setting,
                    headers: {
                        'x-access-token': responseJson.token
                    },
                    data: {
                        categoryid: categoryid,
                    },
                })
                    .then((response) => response.json())
                    .then((responseJson) => {
                        debugger;
                        this.setState({ productlist: responseJson.products });
                        console.log(responseJson);
                    })
                    .catch((error) => {
                        console.log(error);
                    });
            })
            .catch((error) => {
                console.log(error);
            });
    }

    componentDidMount() {
        console.log(this.state.categoryid, 'ffffffffffffffff');
        //this.getCategoryList(0);
        this.getProductList(this.state.categoryid);
    }

    renderCategoryList() {
        return (
            <FlatList
                style={{
                }}
                data={CATEGORY_LIST}
                horizontal={true}
                renderItem={({ item }) => (
                    <View
                        style={[ProductStyles.categorybutton, {
                            borderColor: item.id === this.state.categoryid ? '#84C1E3' : '#FFFFFF',
                        }]}>
                        <TouchableOpacity
                            onPress={() => {
                                this.getProductList(item.id);
                                this.setState({ categoryid: item.id });
                            }}>
                            <Text
                                style={ProductStyles.categorybuttontitle}
                            >{item.title}</Text>
                        </TouchableOpacity>
                    </View>
                )}
                keyExtractor={item => String(item.id)}
            />
        );
    }
    renderRow() {
        return (
            <View style={{
                backgroundColor: 'red',
                width: (screenWidthHeight.width / 2) - 15,
                height: 300,
                marginLeft: 10,
                marginTop: 10
            }}>
                <Text>{(rowData.product_description_list[0]).name} {rowData.price}</Text>
            </View>
        )
    }

    renderProductList() {
        if (this.state.productlist !== null) {
            return (
                <View>
                    <FlatList
                        keyExtractor={item => String(item.product_id)}
                        contentContainerStyle={{
                            flexWrap: 'wrap',
                            flexDirection: 'row',
                        }}
                        data={this.state.productlist}
                        renderItem={({ item }) => (
                            <View style={{
                                backgroundColor: '#FFFFFF',
                                width: (screenWidthHeight.width / 2) - 20,
                                margin: 10,
                                borderRadius: 10,
                                shadowColor: "#666666",
                                shadowOffset: {
                                    width: 0,
                                    height: 0,
                                },
                                shadowOpacity: 0.4,
                                shadowRadius: 10,
                                elevation: 3,
                            }}>
                                <View
                                    style={{
                                        borderTopRightRadius: 10,
                                        borderTopLeftRadius: 10,
                                        overflow: 'hidden',
                                    }}>
                                    <TouchableOpacity
                                        onPress={() => {
                                            this.props.navigation.navigate('ProductDetail', {
                                                productid: item.product_id,
                                                productImage: WEB_PAGE_URL + item.image,
                                                productName: (item.product_description_list[0]).name,
                                                productPrice: item.price
                                            });
                                        }}>
                                        <Image
                                            source={{ uri: WEB_PAGE_URL + item.image }}
                                            style={{
                                                width: (screenWidthHeight.width / 2) - 15,
                                                height: 150,
                                            }} /></TouchableOpacity>
                                </View>
                                <View
                                    style={{
                                        padding: 10,
                                    }}>
                                    <Text
                                        style={{
                                            fontSize: 12,
                                            fontFamily: 'Poppins-Regular',
                                        }}>{(item.product_description_list[0]).name}</Text>
                                    <Text
                                        style={{
                                            fontSize: 17,
                                            fontFamily: 'Poppins-SemiBold',
                                            color: '#397596'
                                        }}>{item.price} CHF</Text>
                                </View>
                            </View>
                        )}
                    />
                </View>
            )
        }
        else {
            return (
                <View><Text>No data!!!</Text></View>
            )
        }
    }

    render() {
        return (
            <View
                style={{
                    backgroundColor: '#E9EAEE',
                    flex: 1,
                }}>
                <StatusBar />
                <Header
                    containerStyle={HeaderStyles.container}
                    leftComponent={
                        <TouchableOpacity
                          onPress={() => {this.props.navigation.goBack(null)}}>
                          <Icon name="chevron-left"  style={HeaderStyles.title} />
                        </TouchableOpacity>
                      }
                    centerComponent={<Text style={HeaderStyles.title}>Products</Text>}
                    rightComponent={
                        <TouchableOpacity
                          onPress={() => { this.props.navigation.dispatch(DrawerActions.openDrawer())}}>
                          <Icon name="bars"  style={HeaderStyles.title} />
                        </TouchableOpacity>
                      }
                    />
                <View
                    style={{
                    }}>
                    {
                        this.renderCategoryList()
                    }
                </View>
                <ScrollView>
                    {
                        this.renderProductList()
                    }
                </ScrollView>
            </View>
        );
    }
}

const mapStateToProps = state => ({
    setting: state.setting,
    auth: state.auth,
    customer: state.customer,
});

export default connect(mapStateToProps)(Index);