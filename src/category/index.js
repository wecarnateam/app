import React, { PureComponent } from 'react';
import { FlatList, View, Text, TouchableOpacity, ScrollView } from 'react-native';
import { connect } from 'react-redux';
import { Button, ButtonGroup, Header, } from 'react-native-elements';
import base64 from 'react-native-base64';
import FontAwesomeIcons from 'react-native-vector-icons/FontAwesome';
import { DrawerActions } from 'react-navigation-drawer';

import { POST,GET } from '../@helper/helper';

import HeaderStyles from '../@assets/header';

class CategoryScreen extends PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            categorylist: null,
        }
    }

    getCategoryList(parentId) {
        GET('/login',{
            setting: this.props.setting,
            authorization: this.props.auth
        })
        .then((response) => response.json())
        .then((responseJson) => {
            console.log(responseJson);
            POST('/categories', {
                setting: this.props.setting,
                headers:{
                    'x-access-token': responseJson.token
                },
                data: {
                    parentid: parentId,
                },
            })
            .then((response) => response.json())
            .then((responseJson) => {
                this.setState({categorylist: responseJson.categories});
                console.log(responseJson);
            })
            .catch((error) => {
                console.log(error);
            });
        })
        .catch((error) => {
          console.log(error);
        });
    }

    componentDidMount(){
        console.log("categoryScreen");
        this.getCategoryList(0);
    }

    
    render() {
        if(this.state.categorylist !== null){
            return (
                <View 
                    style={{ 
                        flex: 1,
                    }}
                >
                   <Header
                    containerStyle={HeaderStyles.container}
                    leftComponent={
                        <TouchableOpacity
                            onPress={() => this.props.navigation.navigate("Home")}>
                            <Text><FontAwesomeIcons name='chevron-left' color='#FFFFFF' size={17} /></Text>
                        </TouchableOpacity>
                    }
                    centerComponent={{ text: 'Category', style: { color: '#F23D3D' } }}
                    rightComponent={{ icon: 'menu', color: '#F23D3D', onPress: () => this.props.navigation.dispatch(DrawerActions.openDrawer()) }} />
                    <View
                        style={{
                            flex:1,
                        }}
                    >
                        <FlatList
                            data={this.state.categorylist}
                            horizontal={true}
                            renderItem={({ item }) => (
                                <View
                                    style={{
                                        borderRadius: 25,
                                        backgroundColor: '#cccccc',
                                        margin: 5,
                                        paddingLeft: 15,
                                        paddingRight: 15,
                                    }}
                                >
                                    <TouchableOpacity
                                        onPress={()=>{
                                            this.props.navigation.navigate('Product', {
                                                categoryid: item.category_id,
                                            });
                                        }}
                                    >
                                        <Text>{(item.category_description_list[0]).meta_title}</Text>
                                    </TouchableOpacity>
                                </View>
                            )}
                            keyExtractor={item => String(item.category_id)}
                        />
                    </View>
                    <View
                        style={{
                            flex:1,
                        }}
                    >
                        
                    </View>
                    <View
                        style={{
                            flex:12,
                        }}
                    ></View>
                </View>
            );
        }
        else{
            return(
                <View><Text>No data!!!</Text></View>
            );
        }
    }
}

const mapStateToProps = state => ({
    setting: state.setting,
    auth: state.auth,
});


export default connect(mapStateToProps)(CategoryScreen);