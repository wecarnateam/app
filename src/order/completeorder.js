import React, { PureComponent } from 'react';
import {
    FlatList, View, Text, TouchableOpacity, TextInput, Switch, Alert, Modal, TouchableWithoutFeedback, ScrollView,
    ActivityIndicator,
} from 'react-native';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Button, ButtonGroup, Header, } from 'react-native-elements';
import { DrawerActions } from 'react-navigation-drawer';
import RNPickerSelect from 'react-native-picker-select';
import FontAwesomeIcons from 'react-native-vector-icons/FontAwesome';
import { showMessage, hideMessage } from 'react-native-flash-message';
import { WebView } from 'react-native-webview';


import { GET, POST } from '../@helper/helper';
import HeaderStyles from '../@assets/header';
import ShoppingCartStyles from '../@assets/shoppingcart';
import StatusBar from '../component/statusbar';
import { WEB_PAGE_URL } from '../static/variables';

class Index extends PureComponent {
    static propTypes = {
        navigation: PropTypes.object.isRequired,
    }

    constructor(props) {
        console.log(props);
        super(props);
        this.state = {
            cartlist: null,
            addresslist: null,
            addressselectiontype: 0,
            addresspickerlist: [],
            selectedaddress: null,
            selectedaddresslabel: "",
            paymenttype: 0,
            ordernote: null,
            aggreement: false,
            showadressmodal: false,
            showtermcondition: false,
            submitting: false,
            ...props.navigation.state.params,
        }
    }

    getCustomerAddress() {
        GET('/login', {
            setting: this.props.setting,
            authorization: this.props.auth,
        })
            .then((response) => response.json())
            .then((responseJson) => {
                console.log(responseJson);
                POST('/addresslistbycustomer', {
                    setting: this.props.setting,
                    headers: {
                        'x-access-token': responseJson.token
                    },
                    data: {
                        customer_id: this.props.customer.customer.customer.customer_id,
                    },
                })
                    .then((response) => response.json())
                    .then((responseJson) => {
                        this.setState({
                            ...responseJson
                        });
                    })
                    .catch((error) => {
                        console.log(error);
                    });
            })
            .catch((error) => {
                console.log(error);
            });
    }

    getTotalCartPrice() {
        let cartTotal = 0;
        if (this.state.cartlist) {
            this.state.cartlist.map((cart, index) => {
                let cartPrice = parseFloat(cart.product.price);
                cart.product_option_list.map((proOpt, index) => {
                    proOpt.product_option_value_list.map((proOptVal, index) => {
                        cartPrice += parseFloat(proOptVal.price);
                    });
                });
                cartTotal += cartPrice * cart.quantity;
            });
        }

        return cartTotal.toFixed(2);
    }

    renderCustomerAddress() {
        if (this.state.addresslist !== null && this.state.addresslist.length !== 0) {
            var addressListContent = this.state.addresslist.map((address, index) => {
                return (
                    <View
                        style={{
                            borderWidth: 1,
                            borderColor: '#EAEAEA',
                            padding: 15,
                        }}>
                        <TouchableOpacity
                            onPress={() => {
                                this.setState({
                                    showadressmodal: false,
                                    selectedaddress: address,
                                    selectedaddresslabel: address.address_1 + ' ' + address.city + ', ' + address.firstname + ' ' + address.lastname
                                });
                            }}>
                            <Text
                                style={{
                                    fontSize: 18,
                                    fontFamily: 'Poppins-Regular',
                                }}>{address.address_1 + ' ' + address.city + ', ' + address.firstname + ' ' + address.lastname}</Text>
                        </TouchableOpacity>
                    </View>
                )
            });
            return (
                <View
                    style={{
                        backgroundColor: '#FFFFFF',
                        width: '80%'
                    }}>
                    <View
                        style={{
                            borderWidth: 1,
                            borderColor: '#EAEAEA',
                            padding: 15,
                        }}>
                        <TouchableOpacity
                            onPress={() => {
                                this.setState({
                                    showadressmodal: false,
                                    selectedaddress: null,
                                    selectedaddresslabel: ""
                                });
                            }}>
                            <Text
                                style={{
                                    fontSize: 18,
                                    fontFamily: 'Poppins-Regular',
                                }}>Select Address</Text>
                        </TouchableOpacity>
                    </View>
                    {
                        addressListContent
                    }
                </View>
            );
        }
        else {
            return (
                <View><Text>Adres yok</Text></View>
            );
        }
    }

    completeOrder() {
        if (this.state.aggreement === false) {
            showMessage({
                icon: "danger",
                message: "Error",
                description: "Accept the PAMPAM distance sales contract!",
                type: "danger",
            });
            this.setState({ submitting: false });
            return 0;
        }

        if (this.state.selectedaddress === null) {
            showMessage({
                icon: "danger",
                message: "Error",
                description: "Select shipping address!",
                type: "danger",
            });
            this.setState({ submitting: false });
            return 0;
        }

        if (this.state.paymenttype === 0) {
            showMessage({
                icon: "danger",
                message: "Error",
                description: "Select payment type!",
                type: "danger",
            });
            this.setState({ submitting: false });
            return 0;
        }

        let orderData = {
            customer_id: this.props.customer.customer.customer.customer_id,
            payment_address_id: this.state.selectedaddress.address_id,
            shipping_address_id: this.state.selectedaddress.address_id,
            comment: (this.state.ordernote === null) ? "" : this.state.ordernote,
            total: this.getTotalCartPrice(),
            cart_list: this.state.cartlist
        }

        GET('/login', {
            setting: this.props.setting,
            authorization: this.props.auth,
        })
            .then((response) => response.json())
            .then((responseJson) => {
                console.log(responseJson);
                POST('/createorder', {
                    setting: this.props.setting,
                    headers: {
                        'x-access-token': responseJson.token
                    },
                    data: orderData,
                })
                    .then((response) => response.json())
                    .then((responseJson) => {
                        showMessage({
                            icon: "success",
                            message: "Success",
                            description: "Order is created successfully!",
                            type: "success",
                        });
                        let propsCustomer = this.props.customer.customer;
                        propsCustomer.cartcount = 0;
                        this.props.updateCartCount(propsCustomer);
                        this.props.navigation.navigate("Order");
                    })
                    .catch((error) => {
                        console.log(error);
                    });
            })
            .catch((error) => {
                console.log(error);
            });
    }

    componentDidMount() {
        this.getCustomerAddress();
    }


    render() {
        return (
            <View
                style={{
                    backgroundColor: '#E9EAEE',
                    flex: 1,
                }}>
                <StatusBar />
                <Header
                    containerStyle={HeaderStyles.container}
                    leftComponent={{ icon: 'arrow-back', type: 'material-icons', color: '#F23D3D', onPress: () => this.props.navigation.goBack() }
                    }
                    centerComponent={{ text: 'Order', style: { color: '#F23D3D' } }}
                    rightComponent={{ icon: 'menu', color: '#F23D3D', onPress: () => this.props.navigation.dispatch(DrawerActions.openDrawer()) }} />
                <ScrollView>
                    <View
                        style={{
                            flex: 10,
                            padding: 20,
                        }}>
                        <View
                            style={{
                                borderRadius: 10,
                                padding: 15,
                                backgroundColor: '#FFFFFF',
                                marginBottom: 15,
                            }}>
                            <View
                                style={{
                                    paddingBottom: 15,
                                }}>
                                <Text
                                    style={{
                                        fontFamily: 'Poppins-Regular',
                                        fontSize: 17,
                                        color: '#397596',
                                        fontWeight: 'bold'
                                    }}>Shipping Address</Text>
                            </View>
                            <View
                                style={{
                                    borderColor: '#CDCDCD',
                                    borderWidth: 1,
                                    borderRadius: 5
                                }}>
                                <TouchableOpacity
                                    onPress={() => {
                                        this.setState({ showadressmodal: true });
                                    }}
                                    style={{
                                        padding: 10,
                                    }}>
                                    <View
                                        style={{
                                            flexDirection: 'row',
                                        }}>
                                        <Text
                                            style={{
                                                alignSelf: 'flex-start',
                                                flex: 1,
                                                fontFamily: 'Poppins-Regular',
                                                fontSize: 16
                                            }}>{this.state.selectedaddresslabel !== "" ? this.state.selectedaddresslabel : "Selected Adres"}</Text>
                                        <Text
                                            style={{
                                                alignSelf: 'center'
                                            }}>
                                            <FontAwesomeIcons name="chevron-down" size={18} color="#CDCDCD"></FontAwesomeIcons>
                                        </Text>
                                    </View>
                                </TouchableOpacity>
                                <Modal animationType='fade' transparent={true} visible={this.state.showadressmodal === true}>
                                    <TouchableWithoutFeedback
                                        onPress={() => {
                                            this.setState({ showadressmodal: false });
                                        }}>
                                        <View
                                            style={{
                                                flex: 1,
                                                backgroundColor: 'rgba(0,0,0,.85)',
                                                justifyContent: 'center',
                                                alignItems: 'center',
                                            }}>
                                            {
                                                this.renderCustomerAddress()
                                            }
                                        </View>
                                    </TouchableWithoutFeedback>
                                </Modal>
                            </View>
                        </View>
                        <View
                            style={{
                                borderRadius: 10,
                                padding: 15,
                                backgroundColor: '#FFFFFF',
                            }}>
                            <View
                                style={{
                                    paddingBottom: 15,
                                }}>
                                <Text
                                    style={{
                                        fontFamily: 'Poppins-Regular',
                                        fontSize: 16,
                                        color: '#397596',
                                        fontWeight: 'bold'
                                    }}>Payment Type</Text>
                            </View>
                            <View
                                style={{
                                    borderColor: '#CDCDCD',
                                    borderWidth: 1,
                                    borderRadius: 5,
                                    marginBottom: 20
                                }}>
                                <RNPickerSelect
                                    placeholder={{ label: 'Select Payment Type', value: 0 }}
                                    style={{
                                        inputAndroid: {
                                            color: '#000000',
                                            fontFamily: 'Poppins-Regular',
                                            fontSize: 16
                                        },
                                        placeholder: {
                                            color: '#000000',
                                            fontFamily: 'Poppins-Regular',
                                            fontSize: 16
                                        },
                                        iconContainer: {
                                            top: 15,
                                            right: 10,
                                        },
                                    }}
                                    placeholderTextColor='#BCBCBC'
                                    value={this.state.paymenttype}
                                    onValueChange={(value) => { this.setState({ paymenttype: value }) }}
                                    items={[
                                        { label: "Cash", value: 1 },
                                        { label: "Credit Card", value: 2 },
                                    ]}
                                    Icon={() => {
                                        return <FontAwesomeIcons name="chevron-down" size={18} color="#CDCDCD" />;
                                    }} />
                            </View>
                            <View
                                style={{
                                    paddingBottom: 15,
                                }}>
                                <Text
                                    style={{
                                        fontFamily: 'Poppins-Regular',
                                        fontSize: 17,
                                        color: '#397596',
                                        fontWeight: 'bold'
                                    }}>Note</Text>
                                <TextInput
                                    style={{
                                        height: 150,
                                        textAlignVertical: 'top',
                                        borderColor: '#CDCDCD',
                                        borderWidth: 0.5,
                                    }}
                                    placeholderTextColor='#BCBCBC'
                                    placeholder="Note"
                                    multiline={true}
                                    numberOfLines={2}
                                    onChangeText={(text) => this.setState({ ordernote: text })}
                                    value={this.state.ordernote} />
                            </View>
                        </View>
                        <View
                            style={{
                                flexDirection: 'row',
                                marginTop: 15,
                            }}>
                            <Switch
                                onValueChange={value => {
                                    this.setState({ aggreement: value });
                                }}
                                value={this.state.aggreement}
                                style={{ marginRight: 5 }} />
                            <View
                                style={{
                                    flex: 1
                                }}>
                                <Text
                                    onPress={() => { 
                                        this.setState({ aggreement: !this.state.aggreement });
                                    }}
                                    style={{
                                        fontSize: 16,
                                        fontFamily: 'Poppins-Regular',
                                    }}>I have read and accept the PAMPAM distance
                                    <Text> </Text>
                                    <Text
                                        onPress={() => {
                                            this.setState({ showtermcondition: true });
                                        }}
                                        style={{
                                            color: '#F23D3D',
                                            textDecorationLine: 'underline',
                                        }}>sales contract.</Text>
                                </Text>
                                <Modal animationType='fade' transparent={true} visible={this.state.showtermcondition === true}>
                                    <View
                                        style={{
                                            flex: 1,
                                            backgroundColor: 'rgba(0,0,0,.85)',
                                            justifyContent: 'center',
                                            alignItems: 'center',
                                        }}>
                                        <View
                                            style={{
                                                backgroundColor: '#FFFFFF',
                                                width: '100%',
                                            }}>
                                            <View
                                                style={{
                                                    backgroundColor: '#FFFFFF00',
                                                    alignContent: 'flex-end',
                                                    justifyContent: 'flex-end'
                                                }}>
                                                <TouchableOpacity
                                                    style={{
                                                        padding: 15,
                                                        backgroundColor: '#FFFFFF00',
                                                        alignSelf: 'flex-end'
                                                    }}
                                                    onPress={() => {
                                                        this.setState({
                                                            showtermcondition: false,
                                                        });
                                                    }}>
                                                    <FontAwesomeIcons name="close" size={18} color="#CDCDCD" />
                                                </TouchableOpacity>
                                            </View>
                                            <View
                                                style={{
                                                    borderWidth: 1,
                                                    borderColor: '#EAEAEA',
                                                    padding: 15,
                                                    height: 500,
                                                }}>
                                                <WebView
                                                    startInLoadingState={true}
                                                    source={{ uri: 'http://pampampizza.ch/condition-generales' }}
                                                />
                                            </View>
                                            <View
                                                style={{
                                                    borderWidth: 1,
                                                    borderColor: '#EAEAEA',
                                                    padding: 15,
                                                }}>
                                                <TouchableOpacity
                                                    onPress={() => {
                                                        this.setState({
                                                            showtermcondition: false,
                                                            aggreement: true
                                                        });
                                                    }}>
                                                    <Text
                                                        style={{
                                                            fontSize: 18,
                                                            fontFamily: 'Poppins-Regular',
                                                            color: '#F23D3D'
                                                        }}>Accept terms and consitions</Text>
                                                </TouchableOpacity>
                                            </View>
                                        </View>
                                    </View>
                                </Modal>
                            </View>
                        </View>
                    </View>
                </ScrollView>
                <View
                    style={{
                        flexDirection: 'row',
                        height: 50,
                    }}>
                    <View
                        style={{
                            flex: 1,
                            backgroundColor: '#FFFFFF',
                            alignItems: 'center',
                            justifyContent: 'center',
                        }}>
                        <Text
                            style={{
                                color: '#9e7538',
                                fontSize: 19,
                                fontFamily: 'Poppins-Regular',
                                fontWeight: 'bold',
                            }}>{this.getTotalCartPrice()} CHF</Text>
                    </View>
                    <View
                        style={{
                            flex: 1,
                            backgroundColor: '#6ace4f',
                        }}>
                        <TouchableOpacity
                            onPress={() => {
                                if (this.state.submitting) {
                                    showMessage({
                                        icon: "warning",
                                        message: "Warning",
                                        description: "Wait for order submitting!",
                                        type: "warning",
                                    });
                                    return 0;
                                }
                                else {
                                    this.setState({ submitting: true }, this.completeOrder);
                                }
                            }}
                            style={{
                                flex: 1,
                                alignItems: 'center',
                                justifyContent: 'center',
                                width: '100%',
                            }}>
                            {
                                !this.state.submitting && <Text
                                    style={{
                                        color: '#FFFFFF',
                                        fontSize: 15,
                                        fontFamily: 'Poppins-Regular',
                                        padding: 15,
                                    }}>Complete Order</Text>
                            }
                            {this.state.submitting && <ActivityIndicator animating={true} size='small' color='#FFFFFF' style={{ paddingRight: 10, }} />}
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        );
    }
}

const mapStateToProps = state => ({
    setting: state.setting,
    auth: state.auth,
    language: state.language,
    customer: state.customer,
});

const mapDispatchToProps = dispatch => ({
    updateCartCount: (customer) => {
        dispatch({
            type: "CUSTOMER",
            payload: customer
        });
    },
});

export default connect(mapStateToProps, mapDispatchToProps)(Index);