import React, { PureComponent } from 'react';
import { FlatList, View, Text, TouchableOpacity, ScrollView } from 'react-native';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Button, ButtonGroup, Header, } from 'react-native-elements';
import { DrawerActions } from 'react-navigation-drawer';
import RadioForm, { RadioButton, RadioButtonInput, RadioButtonLabel } from 'react-native-simple-radio-button';
import RNPickerSelect from 'react-native-picker-select';
import Icon from 'react-native-vector-icons/FontAwesome';

import { GET, POST } from '../@helper/helper';
import HeaderStyles from '../@assets/header';
import OrderStyles from '../@assets/order';
import StatusBar from '../component/statusbar';
import { WEB_PAGE_URL } from '../static/variables';
import CustomerAddress from '../component/address';

class Index extends PureComponent {
    static propTypes = {
        navigation: PropTypes.object.isRequired,
    }

    constructor(props) {
        console.log(props);
        super(props);
        this.state = {
            orderlist: null,
            ...props.navigation.state.params,
        }
    }

    getOrderList() {
        GET('/login', {
            setting: this.props.setting,
            authorization: this.props.auth,
        })
            .then((response) => response.json())
            .then((responseJson) => {
                console.log(responseJson);
                POST('/orderlistbycustomer', {
                    setting: this.props.setting,
                    headers: {
                        'x-access-token': responseJson.token
                    },
                    data: {
                        customer_id: this.props.customer.customer.customer.customer_id,
                    },
                })
                    .then((response) => response.json())
                    .then((responseJson) => {
                        console.log(responseJson);
                        this.setState({
                            ...responseJson
                        });
                    })
                    .catch((error) => {
                        console.log(error);
                    });
            })
            .catch((error) => {
                console.log(error);
            });
    }

    renderOrderList() {
        if (this.state.orderlist !== null) {
            var orderContent = this.state.orderlist.map((order, index) => {
                return (
                    <View
                        style={OrderStyles.container}>
                        <View
                            style={OrderStyles.titlecontainer}>
                            <Text
                                style={OrderStyles.ordertitle}>{order.date_added}</Text>
                            <Text
                                numberOfLines={1}
                                style={(order.order_status.order_status_id === 5) ? OrderStyles.orderstatuscomplete : OrderStyles.orderstatus}>
                                {order.order_status.name}
                            </Text>
                        </View>
                        <View
                            style={OrderStyles.productmaincontainer}>
                            {
                                order.order_product_list.map((orderproduct, index) => {
                                    return (
                                        <View
                                            style={OrderStyles.productcontainer}>
                                            <Text
                                                style={OrderStyles.producttitle}>{orderproduct.name}</Text>
                                            <Text
                                                style={OrderStyles.productcount}>{orderproduct.quantity}</Text>
                                        </View>
                                    )
                                })
                            }
                        </View>
                        <View
                            style={OrderStyles.pricecontainer}>
                            <Text
                                style={OrderStyles.pricetitle}>{order.total} CHF</Text>
                        </View>
                    </View>
                )
            });

            return orderContent;
        }
    }

    componentDidMount() {
        this.getOrderList();
    }

    render() {
        return (
            <View
                style={{
                    backgroundColor: '#E9EAEE',
                    flex: 1,
                }}>
                <StatusBar />
                <Header
                    containerStyle={HeaderStyles.container}
                    leftComponent={
                        <TouchableOpacity
                            onPress={() => { this.props.navigation.goBack(null) }}>
                            <Icon name="chevron-left" style={HeaderStyles.title} />
                        </TouchableOpacity>
                    }
                    centerComponent={<Text style={HeaderStyles.title}>Order</Text>}
                    rightComponent={
                        <TouchableOpacity
                            onPress={() => { this.props.navigation.dispatch(DrawerActions.openDrawer()) }}>
                            <Icon name="bars" style={HeaderStyles.title} />
                        </TouchableOpacity>
                    }
                />
                <View
                    style={{
                        flex: 1,
                    }}>
                    <ScrollView
                        style={{
                            flex: 1,
                            padding: 15,
                            marginBottom: 15,
                        }}>
                        {
                            this.renderOrderList()
                        }
                    </ScrollView>
                </View>
            </View>
        );
    }
}

const mapStateToProps = state => ({
    setting: state.setting,
    auth: state.auth,
    language: state.language,
    customer: state.customer,
});

export default connect(mapStateToProps)(Index);