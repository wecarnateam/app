import React, { PureComponent } from 'react';
import { View, } from 'react-native';


export default ({ renderpage, title, }) => {
    if (renderpage === 'main') {
        return (
            <View>
                <Text>{title}</Text>
            </View>
        );
    }
    else if (renderpage === 'subpage') {
        return (
            <View>
                <Text>{title}</Text>
            </View>
        );
    }
    else {
        return (
            <View>
                <Text>no data!</Text>
            </View>
        );
    }
}