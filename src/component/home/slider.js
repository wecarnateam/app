import React, { Component } from 'react';
import { View, Text, Image, TouchableOpacity, Modal, TouchableWithoutFeedback, } from 'react-native';
import PropTypes from 'prop-types';
import { ParallaxImage } from 'react-native-snap-carousel';
import styles from '../../@assets/home/sliderentry';

export default class SliderEntry extends Component {

    static propTypes = {
        data: PropTypes.object.isRequired,
        even: PropTypes.bool,
        parallax: PropTypes.bool,
        parallaxProps: PropTypes.object,
        clickableimage: PropTypes.bool
    };

    constructor(props) {
        super(props);
        this.state = {
            seeimage: false
        }
    }

    get image() {
        const { data: { illustration }, parallax, parallaxProps, even } = this.props;

        return parallax ? (
            <ParallaxImage
                source={{ uri: illustration }}
                containerStyle={[styles.imageContainer, even ? styles.imageContainerEven : {}]}
                style={styles.image}
                parallaxFactor={0.35}
                showSpinner={true}
                spinnerColor={even ? 'rgba(255, 255, 255, 0.4)' : 'rgba(0, 0, 0, 0.25)'}
                {...parallaxProps}
            />
        ) : (
                <Image
                    source={{ uri: illustration }}
                    style={styles.image}
                />
            );
    }

    render() {
        const { data: { title, subtitle }, even, clickableimage } = this.props;

        return (
            <View>
                <TouchableOpacity
                    activeOpacity={1}
                    style={styles.slideInnerContainer}
                    onPress={() => { clickableimage ? this.setState({ seeimage: true }): console.log('image click'); }}>
                    <View style={styles.shadow} />
                    <View style={[styles.imageContainer, even ? styles.imageContainerEven : {}]}>
                        {this.image}
                    </View>
                </TouchableOpacity>
                <Modal animationType='fade' transparent={true} visible={this.state.seeimage === true}>
                    <TouchableWithoutFeedback onPress={() => this.setState({ seeimage: false })}>
                        <View style={{ flex: 1, justifyContent: 'center', backgroundColor: 'rgba(0,0,0,.85)' }}>
                            <View style={{height: 250}}>
                                {this.image}
                            </View>
                        </View>
                    </TouchableWithoutFeedback>
                </Modal>
            </View>
        );
    }
}
