import React, { PureComponent } from 'react';
import { View, Text, StyleSheet, ScrollView, Modal, TouchableHighlight, } from 'react-native';

class LeftMenu extends PureComponent {
    constructor(props){
        super(props);
        this.state={
            isopenmodal: props.navigation.getParam('isopenmodal'),
        }
    }
    setModalVisible(visible) {
        this.setState({isopenmodal: visible});
      }
    render(){
        return(
            <View style={styles.container}>
            <ScrollView>
            <View>
                <Text style={styles.sectionHeadingStyle}>
                Section 1
                </Text>
                <View style={styles.navSectionStyle}>
                <Text style={styles.navItemStyle} onPress={()=>console.log("page1")}>
                Page1
                </Text>
                </View>
            </View>
            <View>
                <Text style={styles.sectionHeadingStyle}>
                Section 2
                </Text>
                <View style={styles.navSectionStyle}>
                <Text style={styles.navItemStyle} onPress={()=>console.log("page2")}>
                    Page2
                </Text>
                <Text style={styles.navItemStyle} onPress={()=>console.log("page3")}>
                    Page3
                </Text>
                </View>            
            </View>
            <View>
                <Text style={styles.sectionHeadingStyle}>
                Section 3
                </Text>
                <View style={styles.navSectionStyle}>
                <Text style={styles.navItemStyle} onPress={()=>console.log("page4")}>
                Page4
                </Text>
                </View>
            </View>
            </ScrollView>
            <View style={styles.footerContainer}>
            <Text>This is my fixed footer</Text>
            <View style={{marginTop: 22}}>
            <Modal
            animationType="slide"
            transparent={false}
            visible={this.state.isopenmodal}
            onRequestClose={() => {
                Alert.alert('Modal has been closed.');
            }}>
            <View style={{marginTop: 22}}>
                <View>
                <Text>Hello World!</Text>

                <TouchableHighlight
                    onPress={() => {
                    this.setModalVisible(!this.state.isopenmodal);
                    }}>
                    <Text>Hide Modal</Text>
                </TouchableHighlight>
                </View>
            </View>
            </Modal>

            <TouchableHighlight
            onPress={() => {
                this.setModalVisible(true);
            }}>
            <Text>Show Modal</Text>
            </TouchableHighlight>
        </View>
            </View>
        </View>
        )
    }
}

export default LeftMenu;

const styles = StyleSheet.create({
    container: {
        width: 150,
        paddingTop: 20,
        flex: 1,
      },
      navItemStyle: {
        padding: 10
      },
      navSectionStyle: {
        backgroundColor: 'lightgrey'
      },
      sectionHeadingStyle: {
        paddingVertical: 10,
        paddingHorizontal: 5
      },
      footerContainer: {
        padding: 20,
        backgroundColor: 'lightgrey'
      }
});
