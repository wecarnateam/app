import React from 'react'
import { View, StatusBar } from 'react-native'
import { getStatusBarHeight } from '../../@helper/screen'

export default props => {
   const backgroundColor = props.backgroundColor ? props.backgroundColor : '#FFFFFF'
   const barStyle = props.barStyle ? props.barStyle : 'light-content'
   const hidden = props.hidden ? props.hidden : false

   return <View style={[css.headerBar, { backgroundColor }]}>
      <StatusBar backgroundColor={backgroundColor} barStyle={barStyle} hidden={hidden} />
   </View>
}

const css = {
   headerBar: {
      height: getStatusBarHeight(true),
   },
}
