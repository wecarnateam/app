import React, { PureComponent } from 'react';
import { View, } from 'react-native';
import { Header, } from 'react-native-elements';

import HeaderStyle from '../../@assets/header';
import LeftMenuItem from './leftmenuitem';
import RightMenuItem from './rightmenuitem';

export default ({title, navigation}) => {
    return (
        <Header
            containerStyle = {HeaderStyle.container}
            leftComponent={<LeftMenuItem navigation={navigation} />}
            centerComponent={{ text: title, style: HeaderStyle.title }}
            rightComponent={<RightMenuItem />}
        />
    )
}