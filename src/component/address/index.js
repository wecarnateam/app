import React, { PureComponent } from 'react';
import { FlatList, View, Text, TouchableOpacity, ScrollView, StyleSheet } from 'react-native';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Button, ButtonGroup, Header, } from 'react-native-elements';
import { DrawerActions } from 'react-navigation-drawer';
import RadioForm, { RadioButton, RadioButtonInput, RadioButtonLabel } from 'react-native-simple-radio-button';
import RNPickerSelect from 'react-native-picker-select';
import RNModalPicker from "rn-modal-picker";

import { GET, POST } from '../../@helper/helper';
import AddressStyle from '../../@assets/address';
import ShoppingCartStyles from '../../@assets/shoppingcart';
import StatusBar from '../statusbar';
import { WEB_PAGE_URL } from '../../static/variables';
import { TextInput } from 'react-native-gesture-handler';

class Index extends PureComponent {

    constructor(props) {
        super(props);
        this.state = {
            firstname: null,
            lastname: null,
            company: null,
            address_1: null,
            address_2: null,
            city: null,
            postcode: null,
            country_id: 0,
            zone_id: 0,
            countrylist: [],
            searchcountrylist: [],
            searchselectedcountry: {},
            selectedText: '',
            zonelist: [],
            ...this.props,
        }
    }

    getCountrylist() {
        GET('/login', {
            setting: this.props.setting,
            authorization: this.props.auth,
        })
            .then((response) => response.json())
            .then((responseJson) => {
                console.log(responseJson);
                GET('/buildcountrylist', {
                    setting: this.props.setting,
                    authorization: this.props.auth,
                    headers: {
                        'x-access-token': responseJson.token
                    }
                })
                    .then((response) => response.json())
                    .then((responseJson) => {
                        console.log(responseJson);
                        let pickerCountryList = [];
                        let searchdropdowncountrylist = [];
                        responseJson.countrylist.map((data, index) => {
                            pickerCountryList = pickerCountryList.concat({
                                label: data.name,
                                value: data.country_id
                            });
                            searchdropdowncountrylist = searchdropdowncountrylist.concat({
                                id: data.country_id,
                                name: data.name
                            });
                        });
                        this.setState({
                            countrylist: pickerCountryList,
                            searchcountrylist: searchdropdowncountrylist
                        });
                    })
                    .catch((error) => {
                        console.log(error);
                    });
            })
            .catch((error) => {
                console.log(error);
            });
    }

    getZoneList(country_id) {
        console.log(country_id, "asdasdada");
        if (country_id == 0) {
            this.setState({
                zonelist: [],
                zone_id: 0,
            })
        }
        else {
            GET('/login', {
                setting: this.props.setting,
                authorization: this.props.auth,
            })
                .then((response) => response.json())
                .then((responseJson) => {
                    console.log(responseJson);
                    POST('/zonelistbycountryid', {
                        setting: this.props.setting,
                        headers: {
                            'x-access-token': responseJson.token
                        },
                        data: {
                            country_id: country_id
                        },
                    })
                        .then((response) => response.json())
                        .then((responseJson) => {
                            console.log(responseJson);
                            let pickerZoneList = [];
                            responseJson.zonelist.map((data, index) => {
                                pickerZoneList = pickerZoneList.concat({
                                    label: data.name,
                                    value: data.zone_id
                                });
                            });
                            this.setState({
                                zonelist: pickerZoneList
                            });
                        })
                        .catch((error) => {
                            console.log(error);
                        });
                })
                .catch((error) => {
                    console.log(error);
                });
        }
    }

    requiredText(value) {
        if (value == "" || value == null)
            return false;
        else
            return true;
    }

    requiredSelected(value) {
        if (value == 0)
            return false;
        else
            return true;
    }

    componentDidMount() {
        this.getCountrylist();
    }

    render() {
        return (
            <View>
                <TextInput
                    style={[AddressStyle.textinput, !this.requiredText(this.state.firstname) ? css.invalidrequired : null]}
                    value={this.state.firstname}
                    onChangeText={(value) => {
                        this.setState({
                            firstname: value
                        });
                    }}
                    autoCapitalize='none'
                    placeholder="First Name"
                ></TextInput>
                <View style={{ margin: 5, }}></View>
                <TextInput
                    style={[AddressStyle.textinput, !this.requiredText(this.state.lastname) ? css.invalidrequired : null]}
                    value={this.state.lastname}
                    onChangeText={(value) => {
                        this.setState({
                            lastname: value
                        });
                    }}
                    autoCapitalize='none'
                    placeholder="Last Name"
                ></TextInput>
                <View style={{ margin: 5, }}></View>
                <TextInput
                    style={AddressStyle.textinput}
                    value={this.state.company}
                    onChangeText={(value) => {
                        this.setState({
                            company: value
                        });
                    }}
                    autoCapitalize='none'
                    placeholder="Company"
                ></TextInput>
                <View style={{ margin: 5, }}></View>
                <TextInput
                    style={[AddressStyle.textinput, !this.requiredText(this.state.address_1) ? css.invalidrequired : null]}
                    value={this.state.address_1}
                    onChangeText={(value) => {
                        this.setState({
                            address_1: value
                        });
                    }}
                    autoCapitalize='none'
                    placeholder="Address 1"
                ></TextInput>
                <View style={{ margin: 5, }}></View>
                <TextInput
                    style={AddressStyle.textinput}
                    value={this.state.address_2}
                    onChangeText={(value) => {
                        this.setState({
                            address_2: value
                        });
                    }}
                    autoCapitalize='none'
                    placeholder="Address 2"
                ></TextInput>
                <View style={{ margin: 5, }}></View>
                <TextInput
                    style={[AddressStyle.textinput, !this.requiredText(this.state.city) ? css.invalidrequired : null]}
                    value={this.state.city}
                    onChangeText={(value) => {
                        this.setState({
                            city: value
                        });
                    }}
                    autoCapitalize='none'
                    placeholder="City"
                ></TextInput>
                <View style={{ margin: 5, }}></View>
                <TextInput
                    style={AddressStyle.textinput}
                    value={this.state.postcode}
                    onChangeText={(value) => {
                        this.setState({
                            postcode: value
                        });
                    }}
                    autoCapitalize='none'
                    placeholder="Postcode"
                ></TextInput>
                <View style={{ margin: 5, }}></View>
                <View
                    style={[AddressStyle.textinput, !this.requiredSelected(this.state.country_id) ? css.invalidrequired : null]}>
                    <RNPickerSelect
                        placeholder={{ label: 'Select Country', value: 0 }}
                        placeholderTextColor='#BCBCBC'
                        value={this.state.country_id}
                        onValueChange={(value) => {
                            this.setState({ country_id: value });
                            this.getZoneList(value);
                        }}
                        items={this.state.countrylist} />
                </View>
                <View
                    style={[AddressStyle.textinput, !this.requiredSelected(this.state.country_id) ? css.invalidrequired : null]}>
                    <RNModalPicker
                        dataSource={this.state.searchcountrylist}
                        dummyDataSource={this.state.searchcountrylist}
                        defaultValue={false}
                        pickerTitle={"Country Picker"}
                        showSearchBar={true}
                        disablePicker={false}
                        changeAnimation={"none"}
                        searchBarPlaceHolder={"Search....."}
                        showPickerTitle={false}
                        searchBarContainerStyle={this.props.searchBarContainerStyle}
                        pickerStyle={Styles.pickerStyle}
                        pickerItemTextStyle={Styles.listTextViewStyle}
                        selectedLabel={this.state.selectedText}
                        placeHolderLabel='placeholder search country'
                        selectLabelTextStyle={Styles.selectLabelTextStyle}
                        placeHolderTextStyle={Styles.placeHolderTextStyle}
                        selectedValue={(index, item) => this.setState({ selectedText: item.name, searchselectedcountry: item })}
                    />
                </View>
                <View style={{ margin: 5, }}></View>
                <View
                    style={[AddressStyle.textinput, !this.requiredSelected(this.state.zone_id) ? css.invalidrequired : null]}>
                    <RNPickerSelect
                        placeholder={{ label: 'Select Region / State', value: 0 }}
                        placeholderTextColor='#BCBCBC'
                        value={this.state.zone_id}
                        onValueChange={(value) => { this.setState({ zone_id: value }) }}
                        items={this.state.zonelist} />
                </View>
            </View>
        );
    }
}

const css = {
    invalidrequired: {
        borderColor: '#d9534f',
    }
}

const mapStateToProps = state => ({
    setting: state.setting,
    auth: state.auth,
    language: state.language,
    customer: state.customer,
});

export default connect(mapStateToProps)(Index);

const Styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: "center",
        justifyContent: "center"
    },

    searchBarContainerStyle: {
        marginBottom: 10,
        flexDirection: "row",
        height: 40,
        shadowOpacity: 1.0,
        shadowRadius: 5,
        shadowOffset: {
            width: 1,
            height: 1
        },
        backgroundColor: "rgba(255,255,255,1)",
        shadowColor: "#d3d3d3",
        borderRadius: 10,
        elevation: 3,
        marginLeft: 10,
        marginRight: 10
    },

    selectLabelTextStyle: {
        color: "#000",
        textAlign: "left",
        width: "99%",
        padding: 10,
        flexDirection: "row",
        backgroundColor: 'red'
    },
    placeHolderTextStyle: {
        color: "#D3D3D3",
        padding: 10,
        textAlign: "left",
        width: "99%",
        flexDirection: "row",
        backgroundColor: 'red'
    },
    dropDownImageStyle: {
        marginLeft: 10,
        width: 10,
        height: 10,
        alignSelf: "center"
    },
    listTextViewStyle: {
        color: "#000",
        marginVertical: 10,
        flex: 0.9,
        marginLeft: 20,
        marginHorizontal: 10,
        textAlign: "left"
    },
    pickerStyle: {
        marginLeft: 18,
        elevation: 3,
        paddingRight: 25,
        marginRight: 10,
        marginBottom: 2,
        shadowOpacity: 1.0,
        shadowOffset: {
            width: 1,
            height: 1
        },
        borderWidth: 1,
        shadowRadius: 10,
        backgroundColor: "rgba(255,255,255,1)",
        shadowColor: "#d3d3d3",
        borderRadius: 5,
        flexDirection: "row",
        backgroundColor: 'red'
    }
});