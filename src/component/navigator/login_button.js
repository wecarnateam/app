import React, { Component } from 'react';
import { connect } from 'react-redux';
import FontAwesomeIcons from 'react-native-vector-icons/FontAwesome';
import { Text, View, StyleSheet, ImageBackground } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';


class LoginButton extends Component {
    constructor(props) {
        super(props);
        this.state = {
            ...this.props
        }
    }

    isCustomerLogedIn() {
        let isLogin = false;
        if (this.props.customer) {
            if (this.props.customer.customer) {
                if (this.props.customer.customer.cartcount) {
                    isLogin = true;
                }
            }
        }

        return isLogin;
    }

    componentDidMount() {
        console.log(this.props, "login butonnnnnnnnnn");
    }

    render() {
        if (!this.isCustomerLogedIn()) {
            return (
                <View
                    style={{
                        padding: 10,
                    }}>
                    <TouchableOpacity
                        style={{
                            padding: 15,
                            alignItems: 'center',
                            borderRadius: 5,
                            backgroundColor: '#397596',
                        }}
                        onPress={() => {
                            this.props.loginnavigation();
                        }}>
                        <Text
                            style={{
                                color: '#FFFFFF',
                                fontWeight: 'bold',
                                fontFamily: 'Poppins-Regular',
                            }}>Sign In</Text>
                    </TouchableOpacity>
                </View>
            );
        }
        else {
            return (
                null
            );
        }
    }
}


const mapStateToProps = state => ({
    setting: state.setting,
    auth: state.auth,
    customer: state.customer,
});

export default connect(mapStateToProps)(LoginButton);