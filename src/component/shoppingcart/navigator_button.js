import React, { Component } from 'react';
import { connect } from 'react-redux';
import FontAwesomeIcons from 'react-native-vector-icons/FontAwesome';
import { Text, View, StyleSheet, ImageBackground } from 'react-native';


class ShoppingCartButton extends Component {
    constructor(props) {
        super(props);
        this.state = {
            ...this.props
        }
    }

    getCartCount() {
        let cartCount = 0;
        if (this.props.customer) {
            if (this.props.customer.customer) {
                if (this.props.customer.customer.cartcount) {
                    cartCount = this.props.customer.customer.cartcount;
                }
            }
        }

        return cartCount;
    }

    componentDidMount() {
        console.log(this.props, "asdnasdkjskjfbsdjbfsd");
    }

    render() {
        let IconComponent = FontAwesomeIcons;
        let iconName = 'shopping-cart';

        return (
            <View
                style={{
                    backgroundColor: '#397596',
                    padding: 25,
                    position: 'absolute',
                    borderRadius: 50,
                    bottom: 0,
                }}>
                <View
                    style={{
                        position: 'absolute',
                        right: 0,
                        borderColor: '#397596',
                        borderWidth: 1,
                        borderRadius: 50,
                        backgroundColor: '#FFFFFF',
                    }}>
                    <Text
                        style={{
                            fontSize: 12,
                            marginTop: 2,
                            marginBottom: 2,
                            marginLeft: 5,
                            marginRight: 5,
                            color: '#397596',
                        }}>{this.getCartCount()}</Text>
                </View>
                <IconComponent name={this.props.iconname} size={this.props.size} color={this.props.tintcolor} />
            </View>
        );
    }
}


const mapStateToProps = state => ({
    setting: state.setting,
    auth: state.auth,
    customer: state.customer,
});

export default connect(mapStateToProps)(ShoppingCartButton);