export const SIGN_IN = 'SIGN_IN'
export const SIGN_OUT = 'SIGN_OUT'

export const USER_AVATAR = 'USER_AVATAR'
export const USER_LANGUAGE = 'USER_LANGUAGE'
export const USER_NOTIFICATION = 'USER_NOTIFICATION'

export const SETTING = 'SETTING'

export const AUTH = 'AUTH'

export const CUSTOMER = 'CUSTOMER'