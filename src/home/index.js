import React, { PureComponent, Component } from 'react';
import {
    View,
    Text,
    FlatList,
    TouchableOpacity,
    ScrollView,
    TouchableWithoutFeedback,
    Image,
    Dimensions,
    StyleSheet,
} from 'react-native';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { Header } from 'react-native-elements';
import { Left, Right, Icon } from 'native-base';
import { DrawerActions } from 'react-navigation-drawer';


import Carousel from 'react-native-snap-carousel';
import { sliderWidth, itemWidth } from '../@assets/home/sliderentry';
import SliderEntry from '../component/home/slider';
import styles, { colors } from '../@assets/main';

import { POST, GET } from '../@helper/fetch';
import HeaderStyles from '../@assets/header';
import HomeStyles from '../@assets/home';
import StatusBar from '../component/statusbar';

const screenWidthHeight = {
    width: Dimensions.get('screen').width,
    height: Dimensions.get('screen').height,
    categorybuttonwidth: ((Dimensions.get('screen').width - 60) / 3),
    categorybuttonimagewidth: 25,
}

class Index extends PureComponent {
    static propTypes = {
        navigation: PropTypes.object.isRequired,
    }

    constructor(props) {
        super(props)
        this.state = {
            categorylist: null,
            activeSlide: 0,
            carouseldatalist: [
                {
                    title: null,
                    subtitle: null,
                    illustration: 'http://pampam.wecarna.com/image/catalog/revslider_media_folder/pizza-slide.png'
                },
                {
                    title: null,
                    subtitle: null,
                    illustration: 'http://pampam.wecarna.com/image/catalog/revslider_media_folder/pizza-slide2.png'
                },
            ],
        }
    }

    _renderItem({ item, index }) {
        return <SliderEntry data={item} even={true} clickableimage={false} />;
    }

    renderCategoryButton() {
        return (
            <View
                style={{
                    flex: 1,
                    paddingBottom: 50,
                }}>
                <View
                    style={{
                        flexDirection: 'row'
                    }}>
                    <TouchableOpacity
                        onPress={() => {
                            this.props.navigation.navigate('Product', {
                                categoryid: 59,
                            });
                        }}>
                        <View style={HomeStyles.categorybutton}>
                            <Image
                                style={{
                                    width: screenWidthHeight.categorybuttonimagewidth,
                                    height: screenWidthHeight.categorybuttonimagewidth,
                                }}
                                source={require('../@assets/images/home/1-48.png')}
                            ></Image>
                            <Text style={HomeStyles.title}>Pizzas</Text>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity
                        onPress={() => {
                            this.props.navigation.navigate('Product', {
                                categoryid: 60,
                            });
                        }}>
                        <View style={HomeStyles.categorybutton}>
                            <Image
                                style={{
                                    width: screenWidthHeight.categorybuttonimagewidth,
                                    height: screenWidthHeight.categorybuttonimagewidth,
                                }}
                                source={require('../@assets/images/home/2-48.png')}
                            ></Image>
                            <Text style={HomeStyles.title}>Campaigns</Text>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity
                        onPress={() => {
                            this.props.navigation.navigate('Product', {
                                categoryid: 61,
                            });
                        }}>
                        <View style={HomeStyles.categorybutton}>
                            <Image
                                style={{
                                    width: screenWidthHeight.categorybuttonimagewidth,
                                    height: screenWidthHeight.categorybuttonimagewidth,
                                }}
                                source={require('../@assets/images/home/3-48.png')}
                            ></Image>
                            <Text style={HomeStyles.title}>Salades</Text>
                        </View>
                    </TouchableOpacity>
                </View>
                <View
                    style={{
                        flexDirection: 'row'
                    }}>
                    <TouchableOpacity
                        onPress={() => {
                            this.props.navigation.navigate('Product', {
                                categoryid: 62,
                            });
                        }}>
                        <View style={HomeStyles.categorybutton}>
                            <Image
                                style={{
                                    width: screenWidthHeight.categorybuttonimagewidth,
                                    height: screenWidthHeight.categorybuttonimagewidth,
                                }}
                                source={require('../@assets/images/home/4-48.png')}
                            ></Image>
                            <Text style={HomeStyles.title}>Drink</Text>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity
                        onPress={() => {
                            this.props.navigation.navigate('Product', {
                                categoryid: 63,
                            });
                        }}>
                        <View style={HomeStyles.categorybutton}>
                            <View
                                style={{
                                    flexDirection: 'row'
                                }}>
                                <Image
                                    style={{
                                        width: screenWidthHeight.categorybuttonimagewidth,
                                        height: screenWidthHeight.categorybuttonimagewidth,
                                    }}
                                    source={require('../@assets/images/home/5-1-48.png')}
                                ></Image>
                                <Image
                                    style={{
                                        width: screenWidthHeight.categorybuttonimagewidth,
                                        height: screenWidthHeight.categorybuttonimagewidth,
                                    }}
                                    source={require('../@assets/images/home/5-2-48.png')}
                                ></Image>
                            </View>
                            <Text style={HomeStyles.title}>Desserts</Text>
                        </View>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }

    componentDidMount() {
    }

    render() {
        const { navigate } = this.props.navigation;
        return (
            <View
                style={{
                    flex: 1,
                }}>
                <StatusBar />
                <Header
                    containerStyle={HeaderStyles.container}
                    leftComponent={{}}
                    centerComponent={<Image source={require('../@assets/images/logo.png')} />}
                    rightComponent={{ icon: 'menu', type: 'material', color: '#F23D3D', onPress: () => this.props.navigation.dispatch(DrawerActions.openDrawer()) }} />
                <ScrollView
                    style={{
                        backgroundColor: '#E9EAEE',
                    }}>
                    <ScrollView
                        contentContainerStyle={{
                            justifyContent: 'center',
                            alignItems: 'center',
                        }}
                        horizontal={true}
                        pagingEnabled={true}>
                    </ScrollView>
                    <Carousel
                        onSnapToItem={(index) => { this.setState({ activeSlide: index }) }}
                        ref={c => this._slider1Ref = c}
                        data={this.state.carouseldatalist}
                        renderItem={this._renderItem}
                        sliderWidth={sliderWidth}
                        itemWidth={itemWidth}
                        hasParallaxImages={true}
                        firstItem={0}
                        inactiveSlideScale={0.94}
                        inactiveSlideOpacity={0.7}
                        containerCustomStyle={styles.slider}
                        contentContainerCustomStyle={styles.sliderContentContainer}
                        loop={true}
                        autoplay={true}
                    />
                    {
                        this.renderCategoryButton()
                    }
                </ScrollView>
            </View>
        );
    }
}

const mapStatetoProps = state => ({
    setting: state.setting,
    auth: state.auth,
    customer: state.customer,
    language: state.language,
});

export default connect(mapStatetoProps)(Index)