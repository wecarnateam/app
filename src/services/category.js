import React from 'react';

class CategoryService extends React.Component {
    state = {
        categoryList: {}
    }

    constructor(props) {
        super(props);
        fetch('http://10.0.3.2:5858/api/categories', {
            method: 'POST',
            headers: {
                Accept: 'application/json', 
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                parentid: 0
            }),
        })
        .then((response) => response.json())
        .then((responseJson) => {
            this.setState({categoryList: responseJson});
            console.log(responseJson);
        })
        .catch((error) => {
          console.log(error);
        });
    }

    getCategoryList = () => {
        return this.state.categoryList;
    }
}

export default CategoryService;